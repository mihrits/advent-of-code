# Moorits Mihkel Muru 2020
# Advent of Code 2018 day 1: https://adventofcode.com/2018/day/1

using DelimitedFiles
using Test

data = vec(readdlm("input.txt", Int))

# Part 1

println("Solution is: $(sum(data))")


# Part 2


function repeatedfreq(data)
    datasize = size(data,1)
    freq = 0
    freqlist = Set()
    iter = 1

    function next(iter)
        iter + 1 - div(iter, datasize)*datasize
    end # function

    while !in(freq, freqlist)
        push!(freqlist, freq)
        freq += data[iter]
        iter = next(iter)
    end # while
    freq
end # function

println("Solution is: $(repeatedfreq(data))")

testdata = [+1, -2, +3, +1]
println(repeatedfreq(testdata))

# Tests

@testset begin
    @test repeatedfreq([+1, -2, +3, +1]) == 2
    @test repeatedfreq([+1, -1]) == 0
    @test repeatedfreq([+3, +3, +4, -2, -4]) == 10
    @test repeatedfreq([-6, +3, +8, +5, -6]) == 5
    @test repeatedfreq([+7, +7, -2, -7, -4]) == 14
end # testset
