# Moorits Mihkel Muru 2019
# Advent of Code day1 https://adventofcode.com/2019/day/1

using DelimitedFiles


# Fuel needed by the spacecraft for a module with given mass.
function fuelNeeded(mass::Int)::Int
    div(mass, 3) - 2
end

function fuelNeeded(modules::Array{Int})::Array{Int}
    [fuelNeeded(mass) for mass in modules]
end


# To include the extra fuel as well
function extraFuelNeeded(mass::Int)::Int
    total = 0
    while mass >= 9
        mass = fuelNeeded(mass)
        total += mass
    end # while
    total
end # function

function extraFuelNeeded(modules::Array{Int})::Array{Int}
    [extraFuelNeeded(mass) for mass in modules]
end # function

# Test
#print(fuelNeeded([12,14,1969,100756]))
inputFile = "/home/moorits/Documents/gitlab/advent-of-code-2019/day1_part1/input.txt"
inputData = readdlm(inputFile, Int)
#inputData = 100756 # For tests

println("Modules require $(sum(fuelNeeded(inputData))) units of fuel.")


println("Total needed fuel is $(sum(extraFuelNeeded(inputData))) units.")
