# Moorits Mihkel Muru 2019
# Advent of Code day2 https://adventofcode.com/2019/day/2

using DelimitedFiles

function executeIntcode(intcode::Array{Int})::Array{Int}
    id = 1
    tempIntcode = copy(intcode)
    while tempIntcode[id] != 99 && id+3 < maximum(size(tempIntcode))
        if tempIntcode[id] == 1
            tempIntcode[tempIntcode[id+3]+1] = tempIntcode[tempIntcode[id+1]+1] + tempIntcode[tempIntcode[id+2]+1]
        elseif tempIntcode[id] == 2
            tempIntcode[tempIntcode[id+3]+1] = tempIntcode[tempIntcode[id+1]+1] * tempIntcode[tempIntcode[id+2]+1]
        end # if
        id += 4
    end # while
    tempIntcode
end # function

function executeIntcodeVerbose(intcode::Array{Int})::Array{Int}
    id = 1
    tempIntcode = copy(intcode)
    println("Starting!")
    while tempIntcode[id] != 99# && id+3 < maximum(size(tempIntcode))
        if tempIntcode[id] == 1
            println("id = $id, adding $(tempIntcode[intcode[id+1]+1]) and $(tempIntcode[intcode[id+2]+1])")
            tempIntcode[tempIntcode[id+3]+1] = tempIntcode[tempIntcode[id+1]+1] + tempIntcode[tempIntcode[id+2]+1]
        elseif tempIntcode[id] == 2
            println("id = $id, multiplying $(tempIntcode[intcode[id+1]+1]) and $(tempIntcode[intcode[id+2]+1])")
            tempIntcode[tempIntcode[id+3]+1] = tempIntcode[tempIntcode[id+1]+1] * tempIntcode[tempIntcode[id+2]+1]
        end # if
        id += 4
    end # while
    tempIntcode
end # function

###########
# Tests
###########

# test1 = [1,0,0,0,99]
# test2 = [2,3,0,3,99]
# test3 = [2,4,4,5,99,0]
# test4 = [1,1,1,4,99,5,6,0,99]
# test5 = [1,9,10,3,2,3,11,0,99,30,40,50]
#
# testset = [test1, test2, test3, test4, test5]
#
# for test in testset
#     println("$(test) turns to $(executeIntcode(test))")
# end #for

###########
# Execution
###########

inputFile = "/home/moorits/Documents/gitlab/advent-of-code-2019/day2/input.txt"
inputData = readdlm(inputFile, ',', Int)
inputData[2:3] .= (12,2)

#println(executeIntcode(inputData)[1:10])
#println("$(inputData) turns to $(executeIntcode(inputData))")
#println("Difference is $(inputData - executeIntcode(inputData))")

for noun in 0:99
    for verb in 0:99
        memory = copy(inputData)
        println(typeof(memory[2]))
        println(noun)
        memory[2] = noun
        memory[3] = verb
        #memory[2:3] .= (noun,verb)
        if executeIntcode(memory)[1] == 19690720
            println(100*noun + verb)
        end # if
    end # for
end # for
