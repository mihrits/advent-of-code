# Moorits Mihkel Muru 2019
# Advent of Code day 3 https://adventofcode.com/2019/day/3

using DelimitedFiles
using Test

function manhattanDist(points::Tuple{Int,Int})
    sum(abs.(points))
end # function

function getWireCoords(path::Array{SubString{String}})::Array{Tuple{Int,Int}}
    coords = [(0,0)]
    for instruction in path
        dir = instruction[1]
        length = parse(Int, instruction[2:end])
        if dir == 'L'
            for i in 1:length
                push!(coords, (coords[end][1], coords[end][2]-1))
            end # for
        elseif dir == 'R'
            for i in 1:length
                push!(coords, (coords[end][1], coords[end][2]+1))
            end # for
        elseif dir == 'U'
            for i in 1:length
                push!(coords, (coords[end][1]+1, coords[end][2]))
            end # for
        elseif dir == 'D'
            for i in 1:length
                push!(coords, (coords[end][1]-1, coords[end][2]))
            end # for
        end # if
    end # for
    coords
end # function

function getClosestCrossing(path1::Array{SubString{String}},
                            path2::Array{SubString{String}})
    coords1 = getWireCoords(path1)
    coords2 = getWireCoords(path2)
    crossingpoints = coords1[2:end] ∩ coords2[2:end] # excluding the origin point
    (valueMan, indexMan) = findmin(manhattanDist.(crossingpoints))
    (valueSteps, indexSteps) = findmin(indexin(crossingpoints, coords1) .+ indexin(crossingpoints, coords2) .- 2)
    println("Closest by Manhattan distance: $(crossingpoints[indexMan]) $valueMan")
    println("Closest by number of steps: $(crossingpoints[indexSteps]) $valueSteps")
    (crossingpoints[indexMan], valueMan, crossingpoints[indexSteps], valueSteps)
end # function

@testset "Examples" begin
    test11 = split("R75,D30,R83,U83,L12,D49,R71,U7,L72", ',')
    test12 = split("U62,R66,U55,R34,D71,R55,D58,R83", ',')

    test21 = split("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", ',')
    test22 = split("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", ',')

    @test getClosestCrossing(test11,test12)[2] == 159
    @test getClosestCrossing(test21,test22)[2] == 135

    @test getClosestCrossing(test11,test12)[4] == 610
    @test getClosestCrossing(test21,test22)[4] == 410
end # testset

inputData = readdlm("/home/moorits/Documents/gitlab/advent-of-code-2019/day3/input.txt", String)
wire1Path = split(inputData[1], ',')
wire2Path = split(inputData[2], ',')
getClosestCrossing(wire1Path, wire2Path)
