# Moorits Mihkel Muru 2019
# Advent of Code day 4 https://adventofcode.com/2019/day/4

using Test

function adjacentSame(code::Array{Int})::Bool
    for index in 1:size(code)[1]-1
        code[index] == code[index+1] && (return true)
    end # for
    false
end # function

function adjacentPair(code::Array{Int})::Bool
    2 in [count(x->x==i, code) for i in 1:9]
end # function

function decreasingOrder(code::Array{Int})::Bool
    for index in 1:size(code)[1]-1
        code[index] < code[index+1] && (return false)
    end # for
    true
end # function

function countPasswords1(firstInt::Int, lastInt::Int)
    count = 0
    for password in firstInt:lastInt
        passwordDigits = digits(password)
        adjacentSame(passwordDigits) &&
            decreasingOrder(passwordDigits) &&
            (count += 1)
    end # for
    count
end # function

function countPasswords2(firstInt::Int, lastInt::Int)
    count = 0
    for password in firstInt:lastInt
        passwordDigits = digits(password)
        adjacentPair(passwordDigits) &&
            decreasingOrder(passwordDigits) &&
            (count += 1)
    end # for
    count
end # function

@testset "Examples" begin
    @test decreasingOrder(digits(111111))
    @test adjacentSame(digits(111111))
    @test adjacentSame(digits(223450))
    @test decreasingOrder(digits(223450)) == false
    @test adjacentSame(digits(123789)) == false
    @test decreasingOrder(digits(123789))
    @test adjacentPair(digits(112233))
    @test adjacentPair(digits(123444)) == false
    @test adjacentPair(digits(111122))
end # testset

println("The count of password that fit 1st criteria between 124075 and 580769 is $(countPasswords1(124075,580769))")
println("The count of password that fit 2nd criteria between 124075 and 580769 is $(countPasswords2(124075,580769))")
