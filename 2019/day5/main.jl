# Moorits Mihkel Muru 2019
# Advent of Code day 5 https://adventofcode.com/2019/day/5

include("../intcode/Intcode.jl")

inputFileName = "/home/moorits/Documents/gitlab/advent-of-code-2019/day5/input.txt"
inputCode = Intcode.readIntcode(inputFileName)

# part 1
inputValues = [1]
outputCode, outputValues = Intcode.runIntcode(inputCode, inputValues)

# part 2
inputValues = [5]
outputCode, outputValues = Intcode.runIntcode(inputCode, inputValues)
