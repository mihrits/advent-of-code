# Moorits Mihkel Muru 2019
# Advent of Code day 6 https://adventofcode.com/2019/day/6

using DelimitedFiles


inputFileName = "/home/moorits/Documents/gitlab/advent-of-code-2019/day6/input.txt"

function parseInput(input::String)::Array{Tuple{String,String},1}
    output = Tuple{String,String}[]
    splitInput = [split(row, ')') for row in split(input)]

    for row ∈ splitInput
        push!(output, (row[1], row[2]))
    end # for
    output
end # function parseInput

function populateTree(orbits::Array{Tuple{String,String},1})::Dict{String, String}

    orbitsTree = Dict{String, String}()

    for orbit ∈ orbits
        orbitsTree[orbit[2]] = orbit[1]
    end

    orbitsTree
end

function countOrbits(orbitsTree::Dict{String, String})::Int
    count = 0
    for planet ∈ keys(orbitsTree)
        count += 1
        while orbitsTree[planet] != "COM"
            count += 1
            planet = orbitsTree[planet]
        end
    end
    count
end


# Part 1 solution
orbitsString = open(inputFileName) do file
    read(file, String)
end # open
orbitsParsed = parseInput(orbitsString)
orbitsTree = populateTree(orbitsParsed)
orbitsCount = countOrbits(orbitsTree)

println("Number of total orbits is $orbitsCount.")

# Part 2 solution

function traceOrbit(orbitsTree::Dict{String, String} = orbitsTree, orbit::String = "SAN")
    trace = String[]
    while orbit != "COM"
        orbit = orbitsTree[orbit]
        push!(trace, orbit)
    end
    trace
end

function findHighestCommonOrbit(orbitsTree::Dict{String, String} = orbitsTree,
            orbit1::String = "SAN",
            orbit2::String = "YOU")::String
    trace = traceOrbit(orbitsTree, orbit1)
    while orbit2 != "COM"
        orbit2 = orbitsTree[orbit2]
        if orbit2 ∈ trace
            return orbit2
        end
    end
    println("No common orbits before COM")
    orbit2
end

function distanceToGivenOrbit(orbitsTree::Dict{String, String},
            orbitFrom::String,
            orbitTo::String)::Int
    count = 0
    orbit = orbitFrom
    while orbitsTree[orbit] != orbitTo
        count += 1
        orbit = orbitsTree[orbit]
    end
    count
end

function computeMinimalOrbitalTransfersRequired(input, orbit1 = "YOU", orbit2 = "SAN")
    orbitsTree = populateTree(parseInput(input))
    commonOrbit = findHighestCommonOrbit(orbitsTree, orbit1, orbit2)
    distanceToGivenOrbit(orbitsTree, orbit1, commonOrbit) + distanceToGivenOrbit(orbitsTree, orbit2, commonOrbit)
end

s = "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN"

println(computeMinimalOrbitalTransfersRequired(s, "YOU", "SAN") == 4)


reqOrbitalTransfers = computeMinimalOrbitalTransfersRequired(orbitsString)
println("Minimal number of orbital transfers required to get to Santa is $reqOrbitalTransfers")
