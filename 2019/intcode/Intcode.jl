module Intcode

export readIntcode, runIntcode

using DelimitedFiles
using OffsetArrays # Allows to define arrays from index 0


# function i99!() #(code, index, p1, p2)
#     println("Instruction 99: the program has been halted.")
#     break
# end # function

# Functions for different opcode values.
# code - intcode source code
# index - current value of index in the code
# p1, p2 - parameters after the opcode
# input - array of inputValues
# output - array of outputValues

# addition
function i1!(code, index, p1, p2, input, output)
    code[code[index + 3]] = p1 + p2
    4
end # function

# multiplication
function i2!(code, index, p1, p2, input, output)
    code[code[index + 3]] = p1 * p2
    4
end # function

# input
function i3!(code, index, p1, p2, input, output)
    code[code[index + 1]] = pop!(input)
    2
end

# output
function i4!(code, index, p1, p2, input, output)
    append!(output, p1)
    println(p1)
    2
end # function

# if-true-goto
function i5!(code, index, p1, p2, input, output)
    if p1 != 0
        p2 - index
    else
        3
    end # if
end # function

# if-false-goto
function i6!(code, index, p1, p2, input, output)
    if p1 == 0
        p2 - index
    else
        3
    end
end # function

# less-than
function i7!(code, index, p1, p2, input, output)
    if p1 < p2
        code[code[index + 3]] = 1
    else
        code[code[index + 3]] = 0
    end
    4
end # function

# equal-to
function i8!(code, index, p1, p2, input, output)
    if p1 == p2
        code[code[index + 3]] = 1
    else
        code[code[index + 3]] = 0
    end
    4
end # function

instructionTuple = (i1!, i2!, i3!, i4!, i5!, i6!, i7!, i8!)


function readIntcode(inputFileName::String)
    # Gets AoC inputfile, makes it into incode structure (OffsetArray)
    inputData = readdlm(inputFileName, ',', Int)
    OffsetVector(inputData[1,:], 0:(size(inputData, 2) - 1) )
end # function


function runIntcode(inputCode::OffsetArray{Int64,1,Array{Int64,1}}, inputValues=Int[]::Array{Int,1})
    index = 0
    code = copy(inputCode)
    param = Array{Int}(undef, 2)
    inputValues = reverse(inputValues)
    outputValues = Int[]
    while index <= size(code, 1)
        opcodeDigits = digits(code[index], pad = 4)
        opcodeInstr = opcodeDigits[1] + 10*opcodeDigits[2]
        opcodeModes = opcodeDigits[3:end]
        if opcodeInstr == 99
            println("Instruction 99: the program has been halted.")
            break
        end # if

        for i in 1:2
            if opcodeModes[i] == 0
                try
                    param[i] = code[code[index + i]]
                catch
                    param[i] = 0
                end # try
            elseif opcodeModes[i] == 1
                try
                    param[i] = code[index + i]
                catch
                    param[i] = 0
                end # try
            else
                println("ERROR: unknown parameter mode $(opcodeModes[i]), must be 0 or 1.")
            end # elif
        end # for

        index += instructionTuple[opcodeInstr](code, index, param[1], param[2], inputValues, outputValues)
    end # while
    (code, outputValues)
end # function

end  # module Intcode
