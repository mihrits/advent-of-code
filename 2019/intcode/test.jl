using Test
using OffsetArrays

include("Intcode.jl")

function intoOffsetArray(array)
    OffsetArray(array, 0:size(array,1)-1)
end # function

@testset "day2 - addition, multiplication" begin
    in1 = intoOffsetArray([1,9,10,3,2,3,11,0,99,30,40,50])
    out1 = intoOffsetArray([3500,9,10,70,2,3,11,0,99,30,40,50])

    in2 = intoOffsetArray([1,0,0,0,99])
    out2 = intoOffsetArray([2,0,0,0,99])

    in3 = intoOffsetArray([2,3,0,3,99])
    out3 = intoOffsetArray([2,3,0,6,99])

    in4 = intoOffsetArray([2,4,4,5,99,0])
    out4 = intoOffsetArray([2,4,4,5,99,9801])

    in5 = intoOffsetArray([1,1,1,4,99,5,6,0,99])
    out5 = intoOffsetArray([30,1,1,4,2,5,6,0,99])

    @test Intcode.runIntcode(in1)[1] == out1
    @test Intcode.runIntcode(in2)[1] == out2
    @test Intcode.runIntcode(in3)[1] == out3
    @test Intcode.runIntcode(in4)[1] == out4
    @test Intcode.runIntcode(in5)[1] == out5
end

@testset "day5 - equal to, less-than and jump" begin
    in1 = intoOffsetArray([3,9,8,9,10,9,4,9,99,-1,8]) # equal to
    in2 = intoOffsetArray([3,9,7,9,10,9,4,9,99,-1,8]) # less than
    in3 = intoOffsetArray([3,3,1108,-1,8,3,4,3,99]) # equal to with immediate
    in4 = intoOffsetArray([3,3,1107,-1,8,3,4,3,99]) # less than with immediate
    in5 = intoOffsetArray([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]) # if false goto
    in6 = intoOffsetArray([3,3,1105,-1,9,1101,0,0,12,4,12,99,1]) # if true goto with immediate
    in7 = intoOffsetArray([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99])

    @test Intcode.runIntcode(in1, [8])[2] == [1]
    @test Intcode.runIntcode(in1, [7])[2] == [0]

    @test Intcode.runIntcode(in2, [0])[2] == [1]
    @test Intcode.runIntcode(in2, [12412])[2] == [0]

    @test Intcode.runIntcode(in3, [8])[2] == [1]
    @test Intcode.runIntcode(in3, [7])[2] == [0]

    @test Intcode.runIntcode(in4, [0])[2] == [1]
    @test Intcode.runIntcode(in4, [12412])[2] == [0]

    @test Intcode.runIntcode(in5, [0])[2] == [0]
    @test Intcode.runIntcode(in5, [142])[2] == [1]

    @test Intcode.runIntcode(in6, [0])[2] == [0]
    @test Intcode.runIntcode(in6, [124])[2] == [1]

    @test Intcode.runIntcode(in7, [2])[2] == [999]
    @test Intcode.runIntcode(in7, [8])[2] == [1000]
    @test Intcode.runIntcode(in7, [41])[2] == [1001]
end
