# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/10

using AdventOfCode
using Test

input =  map(x -> parse(Int, x), readlines("2020/data/day_10.txt"))

function part_1(input)
    cpy = copy(input)
    adapter_diffs = diff(sort(push!(cpy, 0)))
    count(isequal(1), adapter_diffs) * (count(isequal(3), adapter_diffs) + 1)
end
@info part_1(input)

function part_2(input)
    cpy = copy(input)
    adapter_diffs = diff(sort(push!(cpy, 0)))
    consec_ones = map(length, split(join(adapter_diffs), "3", keepempty=false))
    conversion = Dict(1=>1, 2=>2, 3=>4, 4=>7, 5=>13) # manual deduction :x
    prod(map(x -> conversion[x], consec_ones))
end
@info part_2(input)

testd1 = """
        16
        10
        15
        5
        1
        11
        7
        19
        6
        12
        4
        """

testd2 = """
        28
        33
        18
        42
        31
        14
        46
        20
        48
        47
        24
        23
        49
        45
        19
        38
        39
        11
        1
        32
        25
        35
        8
        17
        7
        9
        4
        2
        34
        10
        3
        """

testd1 = map(x -> parse(Int, x), split(testd1, '\n', keepempty=false))
testd2 = map(x -> parse(Int, x), split(testd2, '\n', keepempty=false))

@testset "Examples" begin
    @test part_1(testd1) == 7 * 5
    @test part_1(testd2) == 22 * 10
    @test part_2(testd1) == 8
    @test part_2(testd2) == 19208
end