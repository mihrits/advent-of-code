# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/11

using AdventOfCode
using Test

input = readlines("2020/data/day_11.txt")

"""
- get_neigbrs
- next_gen_value
- boundry_chk
- struct for data
- floorplan as a dict with seats
"""

struct Seat
    # crd::Tuple
    neigbrs::Tuple
    isfloor::Bool
    isoccupied::Bool
end

function init_floorplan(input)
    floorplan = Dict()
    for i in eachindex(input)
        for j in eachindex(input[i])
            floor = input[i][j] == '.'
            s = Seat(
                    # (i,j),
                    (),
                    input[i][j] == '.',
                    false)
            push!(floorplan, (i,j) => s)
        end
    end
    floorplan
end

# Init the neighbouring crds for every seat removing neighbours that are floor,
# the seat itself, or outside the seating area.
function get_neigbrs(floorplan)
    fp_with_neigbrs = Dict()
    maxcrd = maximum(keys(floorplan))
    for crd in keys(floorplan)
        seat = floorplan[crd]
        seat.isfloor && continue
        neigbrs = vec([(crd .+ (i, j)) for i in -1:1, j in -1:1])
        nreduced = []
        for n in neigbrs
            n[1] < 1 && continue
            n[2] < 1 && continue
            n[1] > maxcrd[1] && continue
            n[2] > maxcrd[2] && continue
            floorplan[n].isfloor && continue
            n == crd && continue
            push!(nreduced, n)
        end
        seat_with_neigbrs = Seat(
                                # seat.crd,
                                Tuple(nreduced),
                                seat.isfloor,
                                seat.isoccupied)
        push!(fp_with_neigbrs, crd => seat_with_neigbrs)
    end
    fp_with_neigbrs
end

function evolve_seat(floorplan, seat::Seat)
    seat.isfloor && return seat

    s = sum(floorplan[neigbr].isoccupied for neigbr in seat.neigbrs)
    if s == 0
        occ = true
    elseif s >= 4
        occ = false
    else
        occ = seat.isoccupied
    end
    Seat(seat.neigbrs, false, occ)
end

function evolve_floorplan(floorplan)
    new_gen_floorplan = Dict()
    for crd in keys(floorplan)
        push!(new_gen_floorplan, crd => evolve_seat(floorplan, floorplan[crd]))
    end
    new_gen_floorplan
end

function part_1(input)
    floorplan_empty = init_floorplan(input)
    fp = get_neigbrs(floorplan_empty)
    while true
        oldfp = fp
        fp = evolve_floorplan(oldfp)
        fp == oldfp && break
    end
    sum(seat.isoccupied for seat in values(fp))
end
@info part_1(input)

function isinbounds(crd, maxcrd)
    (crd[1] >= 1) && (crd[2] >= 1) && (crd[1] <= maxcrd[1]) && (crd[2] <= maxcrd[2])
end

function get_neigbrs2(floorplan)
    fp_with_neigbrs = Dict()
    maxcrd = maximum(keys(floorplan))
    for crd in keys(floorplan)
        seat = floorplan[crd]
        seat.isfloor && continue
        neigbrs_dir = vec([(i, j) for i in -1:1, j in -1:1])
        neigbrs = []
        for dir in neigbrs_dir
            dir == (0,0) && continue
            i = 1
            n = crd .+ dir

            while isinbounds(n, maxcrd) && floorplan[n].isfloor
                i += 1
                n = crd .+ i .* dir
            end

            !(isinbounds(n, maxcrd)) && continue
            push!(neigbrs, n)
        end
        seat_with_neigbrs = Seat(
                                # seat.crd,
                                Tuple(neigbrs),
                                seat.isfloor,
                                seat.isoccupied)
        push!(fp_with_neigbrs, crd => seat_with_neigbrs)
    end
    fp_with_neigbrs
end

function evolve_seat2(floorplan, seat::Seat)
    seat.isfloor && return seat

    s = sum(floorplan[neigbr].isoccupied for neigbr in seat.neigbrs)
    if s == 0
        occ = true
    elseif s >= 5
        occ = false
    else
        occ = seat.isoccupied
    end
    Seat(seat.neigbrs, false, occ)
end

function evolve_floorplan2(floorplan)
    new_gen_floorplan = Dict()
    for crd in keys(floorplan)
        push!(new_gen_floorplan, crd => evolve_seat2(floorplan, floorplan[crd]))
    end
    new_gen_floorplan
end

function part_2(input)
    floorplan_empty = init_floorplan(input)
    fp = get_neigbrs2(floorplan_empty)
    while true
        oldfp = fp
        fp = evolve_floorplan2(oldfp)
        fp == oldfp && break
    end
    sum(seat.isoccupied for seat in values(fp))
end
@info part_2(input)

t = split("""
    L.LL.LL.LL
    LLLLLLL.LL
    L.L.L..L..
    LLLL.LL.LL
    L.LL.LL.LL
    L.LLLLL.LL
    ..L.L.....
    LLLLLLLLLL
    L.LLLLLL.L
    L.LLLLL.LL
    """, "\n", keepempty=false)

@testset "Examples" begin
    @test part_1(t) == 37
    @test part_2(t) == 26
end