# Moorits Mihkel Muru
# https://adventofcode.com/2020/day/12

using AdventOfCode
using Test
using Match

input = readlines("2020/data/day_12.txt")

mutable struct ship_pos
    n::Int
    e::Int
    ang::Int
end

function forward!(pos, val)
    pos.n += round(Int, val * sin(deg2rad(pos.ang)))
    pos.e += round(Int, val * cos(deg2rad(pos.ang)))
    ;
end

function move!(pos, line)
    act = line[1]
    val = parse(Int, line[2:end])
    @match act begin
        'N' => (pos.n += val)
        'S' => (pos.n -= val)
        'E' => (pos.e += val)
        'W' => (pos.e -= val)
        'L' => (pos.ang += val)
        'R' => (pos.ang -= val)
        'F' => forward!(pos, val)
    end
end

function part_1(input)
    pos = ship_pos(0,0,0)
    for line in input
        move!(pos, line)
    end
    abs(pos.n) + abs(pos.e)
end
@info part_1(input)

mutable struct waypoint_pos
    n::Int
    e::Int
end

# TODO
function forward2!(spos, wpos, val)
    spos.n += wpos.n * val
    spos.e += wpos.e * val
    ;
end

function wpos_turn!(wpos, val)
    newn = wpos.n * cos(deg2rad(val)) + wpos.e * sin(deg2rad(val))
    newe = wpos.n * -sin(deg2rad(val)) + wpos.e * cos(deg2rad(val))
    wpos.n = round(Int, newn)
    wpos.e = round(Int, newe)
    ;
end

function move2!(spos, wpos, line)
    act = line[1]
    val = parse(Int, line[2:end])
    @match act begin
        'N' => (wpos.n += val)
        'S' => (wpos.n -= val)
        'E' => (wpos.e += val)
        'W' => (wpos.e -= val)
        'L' => wpos_turn!(wpos, val)
        'R' => wpos_turn!(wpos, -val)
        'F' => forward2!(spos, wpos, val)
    end
end

function part_2(input)
    spos = ship_pos(0,0,0)
    wpos = waypoint_pos(1,10)
    for line in input
        move2!(spos, wpos, line)
        # println(spos)
        # println(wpos)
    end
    abs(spos.n) + abs(spos.e)
end
@info part_2(input)

t = split("""
          F10
          N3
          F7
          R90
          F11""", '\n', keepempty=false)

@testset "Examples" begin
    @test part_1(t) == 25
    @test part_2(t) == 286
end