# Moorits Mihkel Muru
# https://adventofcode.com/2020/day/13

using AdventOfCode
using Test
using Mods
using Dates

input = readlines("2020/data/day_13.txt")

function part_1(input)
    timestamp = parse(Int, input[1])
    buses = parse.(Int, filter(!isequal("x"), split(input[2], ',')))
    closest = (Inf, 0)
    for bus in buses
        wait = (bus - timestamp%bus)
        if wait < closest[1]
            closest = (wait, bus)
        end
    end
    prod(closest)
end
@info part_1(input)

function part_2(input)
    buses = split(input, ',')
    ob = []
    for busnr in eachindex(buses)
        buses[busnr] == "x" && continue
        busid = parse(Int, buses[busnr])
        reminder = busid - (busnr - 1 - busnr%busid * busid)
        push!(ob, Mod{busid}(reminder))        
    end
    CRT(ob...).val

    # sort!(ob, rev=true)
    # bus, offset = popfirst!(ob)
    # i = 1
    # while true
    #     ts = i * bus - offset
    #     for bustime in ob
    #         if (ts+bustime[2])%bustime[1] != 0
    #             break
    #         elseif bustime == ob[end]
    #             return ts + 1
    #         end
    #     end
    #     i += 1
    #     if log10(i)%1 == 0
    #         fmt = "HH:MM:SS"
    #         println("i = $i at $(Dates.format(now(), fmt))")
    #     end
    # end
end
@info part_2(input[2])

t = split("""
939
7,13,x,x,59,x,31,19""", '\n', keepempty=false)

@testset "Examples" begin
    @test part_1(t) == 295
    @test part_2(t[2]) == 1068781
    @test part_2("17,x,13,19") == 3417
    @test part_2("67,7,59,61") == 754018
    @test part_2("67,x,7,59,61") == 779210
    @test part_2("67,7,x,59,61") == 1261476
    @test part_2("1789,37,47,1889") == 1202161486
end
