# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/15

using AdventOfCode
using Test
using ProgressMeter

input = readlines("2020/data/day_15.txt")[1]

function part_1(input)
    nums = reverse(map(x -> parse(Int, x), split(input, ',')))
    new = popfirst!(nums)
    for _ in length(nums):2019
        dist = findfirst(isequal(new), nums)
        if isnothing(dist)
            dist = 0
        end
        pushfirst!(nums, new)
        new = dist
    end
    nums[begin]
end
@info part_1(input)

# function part_2(input)
#     nums = reverse(map(x -> parse(Int, x), split(input, ',')))
#     new = popfirst!(nums)
#     @showprogress for _ in length(nums):30000000-1
#         dist = findfirst(isequal(new), nums)
#         if isnothing(dist)
#             dist = 0
#         end
#         pushfirst!(nums, new)
#         new = dist
#     end
#     nums[begin]
# end

function part_2(input)
    srtnum = map(x -> parse(Int, x), split(input, ','))
    new = pop!(srtnum)
    nums = Dict()
    for idx in eachindex(srtnum)
        push!(nums, srtnum[idx] => idx)
    end
    @showprogress for i in length(nums)+1:30000000-1
        if new in keys(nums)
            dist = i - nums[new]
        else
            dist = 0
        end
        # println("nums = $nums")
        nums[new] = i
        new = dist
    end
    new
end
@info part_2(input)

testset = [
    ("0,3,6", 436, 175594)  ,
    ("1,3,2", 1, 2578)      ,
    ("2,1,3", 10, 3544142)  ,
    ("1,2,3", 27, 261214)   ,
    ("2,3,1", 78, 6895259)  ,
    ("3,2,1", 438, 18)      ,
    ("3,1,2", 1836, 362)    ,
]

@testset "Examples" begin
    for test in testset
        @test part_1(test[1]) == test[2]
        @test part_2(test[1]) == test[3]
    end
    # @test part_1(testset[1][1]) == testset[1][2]
end
