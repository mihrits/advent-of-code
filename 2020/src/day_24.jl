# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/24

using AdventOfCode
using Test
using Match

input = readlines("2020/data/day_24.txt")

function chgpos(pos, d)
    chg = @match d begin
        'e' => (1, 0)
        'w' => (-1, 0)
        "ne" => (1, -1)
        "nw" => (0, -1)
        "se" => (0, 1)
        "sw" => (-1, 1)
    end
    pos .+ chg
end

function part_1(input)
    tiles = Set()
    for line in input
        dirs = collect(line)
        pos = [0,0]
        while length(dirs) > 0
            d = popfirst!(dirs)
            if (d == 'n') || (d == 's')
                d *= popfirst!(dirs)
            end
            pos = chgpos(pos, d)
        end
        if pos in tiles
            delete!(tiles, pos)
        else
            push!(tiles, pos)
        end
    end
    length(tiles)
end
@info part_1(input)

function getneibrs(pos)
    neibrs = [[1,0], [-1,0], [0,1], [0,-1], [1,-1], [-1,1]]
    [pos .+ neibr for neibr in neibrs]
end

function part_2(input)
    tiles = Set()
    for line in input
        dirs = collect(line)
        pos = [0,0]
        while length(dirs) > 0
            d = popfirst!(dirs)
            if (d == 'n') || (d == 's')
                d *= popfirst!(dirs)
            end
            pos = chgpos(pos, d)
        end
        if pos in tiles
            delete!(tiles, pos)
        else
            push!(tiles, pos)
        end
    end

    for _ in 1:100
        newtiles = Set()
        for t in tiles
            tn = getneibrs(t)
            blcn = sum(in(tiles), tn)
            if (blcn == 1) || (blcn == 2)
                push!(newtiles, t)
            end
            for t_ in tn
                tnn = getneibrs(t_)
                if sum(in(tiles), tnn) == 2
                    push!(newtiles, t_)
                end
            end
        end
        tiles = newtiles
    end
    length(tiles)
end
@info part_2(input)

t = split("""
sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew""", '\n', keepempty=false)

@testset "Examples" begin
    @test part_1(t) == 10
    @test part_2(t) == 2208
end
