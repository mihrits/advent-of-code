# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/3


using AdventOfCode
using Test

input = readlines("2020/data/day_3.txt")

function periodic_boundry(crd, boundry)
    newcrd = crd
    while newcrd > boundry
        newcrd -= boundry
    end
    newcrd
end

function istree(x, y, routemap)
    routemap[y][periodic_boundry(x, length(routemap[1]))] == '#'
end

function count_trees(step_r, step_d, routemap)
    cnt = 0

    for i in 0:step_d:length(routemap)-1
        if istree(step_r*i÷step_d + 1, i + 1, routemap)
            cnt += 1
        end
    end
    cnt
end

function part_1(input)
    # cnt = 0
    # let i = 1
    #     for line in input
    #         if line[i] == '#'
    #             cnt += 1
    #         end
    #         i += 3
    #         if i > length(line)
    #             i -= length(line)
    #         end
    #     end
    # end

    cnt = count_trees(3,1,input)
    cnt
end
@info part_1(input)

function part_2(input)
    slopes = (
        (1,1),
        (3,1),
        (5,1),
        (7,1),
        (1,2),
    )
    counts = Int[]
    for slope in slopes
        step_r, step_d = slope
        push!(counts, count_trees(step_r, step_d, input))    
    end
    prod(counts)
end
@info part_2(input)


@testset "Examples" begin
    testdata =  """
                ..##.......
                #...#...#..
                .#....#..#.
                ..#.#...#.#
                .#...##..#.
                ..#.##.....
                .#.#.#....#
                .#........#
                #.##...#...
                #...##....#
                .#..#...#.#
                """
    testdata = split(testdata, "\n")[begin:end-1]
    @test part_1(testdata) == 7
    @test part_2(testdata) == 336
end
