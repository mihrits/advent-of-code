# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/5

using AdventOfCode
using Test

input = readlines("2020/data/day_5.txt")

function seatname_to_rowcol(seatname)
    seatname_binary = replace(collect(seatname), 'F'=>'0', 'B'=>'1', 'L'=>'0', 'R'=>'1')
    row = parse(Int, prod(seatname_binary[begin:7]), base=2)
    column = parse(Int, prod(seatname_binary[8:end]), base=2)
    (row, column)
end

function rowcol_to_ID(row, column)
    row * 8 + column
end

function seatname_to_ID(seatname)
    rowcol_to_ID(seatname_to_rowcol(seatname)...)
end

function part_1(input)
    max = 0
    for line in input
        ID = seatname_to_ID(line)
        if ID > max
            max = ID
        end
    end
    max
end
@info part_1(input)

function part_2(input)
    IDs = []
    for line in input
        push!(IDs, seatname_to_ID(line))
    end
    fullIDs = Set(minimum(IDs):maximum(IDs))
    myseatID = setdiff(fullIDs, Set(IDs))
    myseatID
end
@info part_2(input)

@testset "Examples" begin
    @test seatname_to_ID("FBFBBFFRLR") == 357
    @test seatname_to_ID("BFFFBBFRRR") == 567
    @test seatname_to_ID("FFFBBBFRRR") == 119
    @test seatname_to_ID("BBFFBBFRLL") == 820
end