# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/6

using AdventOfCode
using Test

# hack to remove unneccesary line break from last line
input = split(read("2020/data/day_6.txt", String)[begin:end-1], "\n\n")

function count_answers(group)
    length(delete!(Set(group), '\n'))
end

function part_1(input)
    mapreduce(count_answers, +, input)
end
@info part_1(input)

function count_answers2(group)
    answers = delete!(Set(group), '\n')
    ppls = count(==('\n'), group) + 1
    mapreduce(x -> count(==(x), group)==ppls, +, answers)
end

function part_2(input)
    mapreduce(count_answers2, +, input)
end
@info part_2(input)

testdata =  """
            abc

            a
            b
            c

            ab
            ac

            a
            a
            a
            a

            b
            """
testdata = split(testdata[begin:end-1], "\n\n")

@testset "Examples" begin
    @test part_1(testdata) == 11
    @test part_2(testdata) == 6
end