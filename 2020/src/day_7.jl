# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/7

using AdventOfCode
using Test

input = readlines("2020/data/day_7.txt")

function recursive_search!(dict, key)
    if dict[key] isa Tuple
        for bagname in dict[key]
            dict[key] = recursive_search!(dict, bagname)
            if dict[key] == true
                break
            end
        end
    end
    dict[key]
end

function bags_to_dict(input)
    bags = Dict()
    for line in input
        name, fullcontents = split(line, " bags contain ")
        if name == "shiny gold"
            continue
        end
        if contains(fullcontents, "no ")
            push!(bags, name => false)
        else
            contents = collect(eachmatch(r"(\d+)\s(\w+\s\w+)", fullcontents))
            contentslist = Tuple(contents[bagnr][2] for bagnr in eachindex(contents))
            if "shiny gold" in contentslist
                push!(bags, name => true)
            else
                push!(bags, name => contentslist)
            end
        end
    end
    bags
end

function part_1(input)
    # Parse data into dict
    bags = bags_to_dict(input)

    # If a bag can contain "shiny gold" => true, if not => false
    for key in keys(bags)
        recursive_search!(bags, key)
    end
    count(values(bags))
end
@info part_1(input)

struct bag
    name::String
    contents
    number::Int
end

# function bags_to_struct(input)
#     bagslist = bag[]
#     for line in input
#         name, fullcontents = split(line, " bags contain ")
#         if contains(fullcontents, "no ")
#             push!(bags, Bags(name, None, 0))
#         else
#             contents = collect(eachmatch(r"(\d+)\s(\w+\s\w+)", fullcontents))
#     end
# end

function bags_to_dict2(input)
    bags = Dict()
    for line in input
        name, fullcontents = split(line, " bags contain ")
        if contains(fullcontents, "no ")
            push!(bags, name => 0)
        else
            contents = collect(eachmatch(r"(\d+)\s(\w+\s\w+)", fullcontents))
            contentslist = Dict(contents[bagnr][2] => parse(Int, contents[bagnr][1]) for bagnr in eachindex(contents))
            push!(bags, name => contentslist)
        end
    end
    bags
end

function recursive_count!(dict, key, mult)
    if dict[key] isa Dict
        dict[key] = sum(x -> recursive_count!(dict, x, dict[key][x]), keys(dict[key]))
    end
    # if mult isa Dict
    #     mult = dict[key]
    # end
    # println("Name: $(key) \t\t value: $(dict[key]) \t\t mult: $(mult)")
    return mult * dict[key] + mult
end

function part_2(input)
    bags = bags_to_dict2(input)
    recbags = recursive_count!(bags, "shiny gold", 1)
    # println(bags)
    recbags - 1
end
@info part_2(input)

testd = """
        light red bags contain 1 bright white bag, 2 muted yellow bags.
        dark orange bags contain 3 bright white bags, 4 muted yellow bags.
        bright white bags contain 1 shiny gold bag.
        muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
        shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
        dark olive bags contain 3 faded blue bags, 4 dotted black bags.
        vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
        faded blue bags contain no other bags.
        dotted black bags contain no other bags.
        """

testd2 = """
        shiny gold bags contain 2 dark red bags.
        dark red bags contain 2 dark orange bags.
        dark orange bags contain 2 dark yellow bags.
        dark yellow bags contain 2 dark green bags.
        dark green bags contain 2 dark blue bags.
        dark blue bags contain 2 dark violet bags.
        dark violet bags contain no other bags.
        """

testd = split(testd, '\n'; keepempty=false)
testd2 = split(testd2, '\n'; keepempty=false)

@testset "Examples" begin
   @test part_1(testd) == 4
   @test part_2(testd) == 32
   @test part_2(testd2) == 126
end