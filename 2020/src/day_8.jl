# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/8

using AdventOfCode
using Test

input = readlines("2020/data/day_8.txt")

function part_1(input)
    visited = Set{Int}()
    acc = 0
    i = 1
    while !(i in visited)
        push!(visited, i)
        ins, arg = split(input[i])
        if ins == "acc"
            acc += parse(Int, arg)
        elseif ins == "jmp"
            i += parse(Int, arg) - 1 
        end
        i += 1
    end
    acc
end
@info part_1(input)

function runbootcode(input)
    acc = 0
    inputlen = length(input)
    visited = Set{Int}()
    i = 1
    while !(i in visited)
        push!(visited, i)
        ins, arg = split(input[i])
        if ins == "acc"
            acc += parse(Int, arg)
        elseif ins == "jmp"
            i += parse(Int, arg) - 1 
        end
        i += 1
        if i == inputlen + 1
            return (finished=true, acc=acc)
        end
    end
    return (finished=false, acc=acc)
end

# function part_2(input)
#     numofnop = sum(x -> occursin("nop", x), input)
#     numofjmp = sum(x -> occursin("jmp", x), input)
#     joinedinput = join(input, "||")
#     for i in 1:numofjmp
#         newinput = replace(replace(joinedinput, "jmp"=>"nop"; count=i), "nop"=>"jmp"; count=i-1)
#         result = runbootcode(newinput)
#         if result.finished
#             return result.acc
#         end            
#     end
#     for i in 1:numofnop
#         newinput = replace(replace(joinedinput, "nop"=>"jmp"; count=i),"jmp"=>"nop" ; count=i-1)
#         result = runbootcode(newinput)
#         if result.finished
#             return result.acc
#         end            
#     end
#     return -9999
# end

function part_2(input)
    for i in 1:length(input)
        ins, arg = split(input[i])
        newinput = copy(input)
        if ins == "nop"
            newinput[i] = "jmp "*arg
            res = runbootcode(newinput)
            if res.finished
                return res.acc
            end
        elseif ins == "jmp"
            newinput[i] = "nop "*arg
            res = runbootcode(newinput)
            if res.finished
                return res.acc
            end
        end
    end
end
@info part_2(input)

testd = split("""
            nop +0
            acc +1
            jmp +4
            acc +3
            jmp -3
            acc -99
            acc +1
            jmp -4
            acc +6
            """, '\n', keepempty = false)

@testset "Examples" begin
    @test part_1(testd) == 5
    @test part_2(testd) == 8
end