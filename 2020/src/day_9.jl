# Moorits Mihkel Muru 2020
# https://adventofcode.com/2020/day/9

using AdventOfCode
using Test

input = readlines("2020/data/day_9.txt")

function part_1(input, len)
    input = map(x -> parse(Int, x), input)
    queue = input[1:len]
    for i in len+1:length(input)
        for el in queue
            tmpqueue = deleteat!(copy(queue), findfirst(isequal(el), queue))
            if (input[i] - el) in tmpqueue
                popfirst!(queue)
                push!(queue, input[i])
                break
            end
            if el == queue[end]
                return input[i]
            end
        end
    end
end
@info part_1(input, 25)
# Answer = 731031916

function part_2(input, target)
    input = map(x -> parse(Int, x), input)
    queue = Int64[]
    for el in input
        push!(queue, el)

        while sum(queue) > target
            popfirst!(queue)
        end

        if sum(queue) == target
            return minimum(queue) + maximum(queue)
        end
    end
end
@info part_2(input, 731031916)

testd = split("""
            35
            20
            15
            25
            47
            40
            62
            55
            65
            95
            102
            117
            150
            182
            127
            219
            299
            277
            309
            576
            """, '\n', keepempty=false)

@testset "Examples" begin
    @test part_1(testd,5) == 127
    @test part_2(testd, 127) == 62
end