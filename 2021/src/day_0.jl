# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/XX

using ReTest

input = readlines("2021/data/day_XX.txt")
testinput = readlines("2021/data/day_XX_test.txt")

function part_1(input)
    nothing
end
# @show part_1(input)

function part_2(input)
    nothing
end
# @show part_2(input)

@testset "dayXX" begin
    @test part_1(testinput) == nothing
    @test part_2(testinput) == nothing
end
retest()