# Moorits Mihkel Muru
# https://adventofcode.com/2021/day/1
using AdventOfCode
using ReTest

input = readlines("2021/data/day_1.txt")

function part_1(input)
    depths = parse.(Int, input)
	count(>(0), diff(depths))
end
@info part_1(input)

part_1mathy(input) = parse.(Int, input) |> diff .|> >(0) |> sum


function part_2(input)
	depths = parse.(Int, input)
	moving_window_sums = depths[1:end-2] + depths[2:end-1] + depths[3:end]
	count(>(0), diff(moving_window_sums))
end
@info part_2(input)

function part_2clever(input)
	depths = parse.(Int, input)
	count = 0
	for i in 1:length(depths)-3
		count += depths[i] < depths[i+3]
	end
	count
end


@testset "day01" begin
    testinput = readlines(IOBuffer("""199
	200
	208
	210
	200
	207
	240
	269
	260
	263"""))
	@test part_1(testinput) == 7
	@test part_1mathy(testinput) == 7
	@test part_2(testinput) == 5
	@test part_2clever(testinput) == 5
end
