# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/10

using ReTest
using Base.Iterators: peel

input = readlines("2021/data/day_10.txt")
testinput = readlines("2021/data/day_10_test.txt")

const right = Dict(
    '(' => ')',
    '[' => ']',
    '{' => '}',
    '<' => '>',
)

const scores = Dict(
    ')' => 3,
    ']' => 57,
    '}' => 1197,
    '>' => 25137,
)

function part_1(input)
    score = 0
    for line in input
        open = Vector{Char}()
        curr, rest = peel(collect(line))
        for char in rest
            if char in values(right)
                if char == right[curr]
                    !isempty(open) ? curr = pop!(open) : curr = ' '
                    continue
                end
                score += scores[char]
                break
            end
            push!(open, curr)
            curr = char
        end
    end
    score      
end
@show part_1(input)

const scores2 = Dict(
    '(' => 1,
    '[' => 2,
    '{' => 3,
    '<' => 4,
)

function part_2(input)
    score = Vector{Int}()
    for line in input
        corr = false
        open = Vector{Char}()
        curr, rest = peel(collect(line))
        for char in rest
            if char in values(right)
                if char != right[curr]
                    corr = true
                    break
                end
                !isempty(open) ? curr = pop!(open) : curr = ' '
                continue
            end
            curr != ' ' && push!(open, curr)
            curr = char
        end
        if !corr
            line_score = 0
            push!(open, curr)
            while !isempty(open)
                line_score = 5*line_score + scores2[pop!(open)]
            end
            push!(score, line_score)
        end
    end
    sort!(score)
    score[ceil(Int, end/2)]
end
@show part_2(input)

@testset "day10" begin
    @test part_1(testinput) == 26397
    @test part_2(testinput) == 288957
end