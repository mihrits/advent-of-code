# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/11

using ReTest

const CI = CartesianIndex
const CIs = CartesianIndices


input = readlines("2021/data/day_11.txt")
testinput = readlines("2021/data/day_11_test.txt")

parse_grid(input) = permutedims(mapreduce(x -> parse.(Int, collect(x)), hcat, input))

neighbors(crd) = [crd + Δ for Δ in CIs((-1:1, -1:1))]

function flash!(grid, crd)
    for oct in neighbors(crd)
        if oct in CIs(grid)
            grid[oct] += 1
        end
    end
    grid[crd] = -9
end


function part_1(input, steps)
    grid = parse_grid(input)
    c = 0
    for _ in 1:steps
        grid .+= 1
        while any(grid .> 9)
            crd = findfirst(>(9), grid)
            flash!(grid, crd)
            c += 1
        end
        for flashed in findall(<(0), grid)
            grid[flashed] = 0
        end
        # @show grid
    end
    c
end
@show part_1(input, 100)

function part_2(input)
    grid = parse_grid(input)
    c = 0
    while true
        c += 1
        grid .+= 1
        while any(grid .> 9)
            crd = findfirst(>(9), grid)
            flash!(grid, crd)
        end
        for flashed in findall(<(0), grid)
            grid[flashed] = 0
        end
        if all(grid .== 0)
            return c
        end
    end
end
@show part_2(input)

@testset "day11" begin
    @test part_1(testinput, 10) == 204
    @test part_1(testinput, 100) == 1656
    @test part_2(testinput) == 195
end