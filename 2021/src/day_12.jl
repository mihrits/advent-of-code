# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/12

using ReTest
using StatsBase: countmap

input = readlines("2021/data/day_12.txt")
testinput1 = readlines("2021/data/day_12_test1.txt")
testinput2 = readlines("2021/data/day_12_test2.txt")
testinput3 = readlines("2021/data/day_12_test3.txt")

# abstract type Cave end 

struct Cave#S <: Cave
    c::Vector{Cave}
end

function nr_of_paths(caves, path=["start"])
    last(path) == "end" && return  1

    routes = Vector{String}()

    for route in caves[last(path)]
        islowercase(route[1]) && route in path || push!(routes, route)
    end

    isempty(routes) && return 0

    return sum(nr_of_paths(caves, push!(copy(path), next)) for next in routes)
end

function parse_input(input)
    connections = [Set(split(line, "-")) for line in input]
    allcaves = setdiff!(reduce(∪, connections), Set(["end"]))
    caves = Dict{String, Set{String}}()
    for cave in allcaves
        ids = findall(x -> cave in x, connections)
        children = setdiff!(Set(reduce(append!, connections[ids], init=String[])), Set([cave, "start"]))
        push!(caves, cave => children)
    end
    caves
end

function part_1(input)
    input |> parse_input |> nr_of_paths
end
@show part_1(input)

function nr_of_paths2(caves, path=["start"])
    last(path) == "end" && return  1

    routes = Vector{String}()

    for route in caves[last(path)]
        if islowercase(route[1]) &&
            route in path &&
            any(values(countmap(path[map(s -> islowercase(s[1]), path)])) .> 1)
            continue
        end
        push!(routes, route)
    end

    isempty(routes) && return 0

    return sum(nr_of_paths2(caves, push!(copy(path), next)) for next in routes)
end

function part_2(input)
    input |> parse_input |> nr_of_paths2
end
@show part_2(input)

@testset "day12" begin
    @test part_1(testinput1) == 10
    @test part_1(testinput2) == 19
    @test part_1(testinput3) == 226
    @test part_2(testinput1) == 36
    @test part_2(testinput2) == 103
    @test part_2(testinput3) == 3509
end
