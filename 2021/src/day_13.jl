# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/13

using ReTest

const CI = CartesianIndex

input = readlines("2021/data/day_13.txt")
testinput = readlines("2021/data/day_13_test.txt")


function parse_input(input)
    dots = Vector{CI}()
    folds = Vector{CI}()
    sep = findfirst(==(""), input)

    for line in input[1:sep-1]
        a, b = parse.(Int, split(line, ","))
        push!(dots, CI(a,b))
    end

    for line in input[sep+1:end]
        val = parse(Int, match(r"\d+", line).match)
        if 'y' in line
            push!(folds, CI(0, val))
        else
            push!(folds, CI(val, 0))
        end
    end
    Set(dots), folds
end

function part_1(input)
    dots, folds = parse_input(input)
    fold = popfirst!(folds)
    ndots = Set{CI}()
    for d in dots
        if all((d-fold).I .>= 0)
            push!(ndots, CI(abs.((2*fold - d).I)))
        else
            push!(ndots, d)
        end
    end
    length(ndots)        
end
@show part_1(input)

function part_2(input)
    dots, folds = parse_input(input)
    for fold in folds
        ndots = Set{CI}()
        for d in dots
            if all((d-fold).I .>= 0)
                push!(ndots, CI(abs.((2*fold - d).I)))
            else
                push!(ndots, d)
            end
        end
        dots = ndots
    end
    dots
end
# @show part_2(input)

@testset "day13" begin
    @test part_1(testinput) == 17
end