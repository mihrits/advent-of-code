# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/14

using ReTest

input = readlines("2021/data/day_14.txt")
testinput = readlines("2021/data/day_14_test.txt")

function parser(input)
    template = collect(input[1])
    pairs = [prod(template[i:i+1]) for i in 1:length(template)-1]
    rules = Dict{String, Char}()
    for line in input[3:end]
        a, b = split(replace(line, " -> " => " "))
        push!(rules, a => only(b))
    end
    counts = Dict(key => 0 for key in keys(rules))
    for pair in pairs
        counts[pair] += 1
    end
    pair_rules = Dict(key => (first(key)*rules[key], rules[key]*last(key)) for key in keys(rules))
    counts, pair_rules, unique(values(rules)), (first(template), last(template))
end

function poly_expand!(counts, pair_rules)
    new_counts = Dict(k => 0 for k in keys(counts))
    for key in keys(counts)
        key1, key2 = pair_rules[key]
        new_counts[key1] += counts[key]
        new_counts[key2] += counts[key]
    end
    new_counts
end

function count_letters(counts, unique_letters, temp_first_last)
    temp_first_last
    letter_counts = Dict(u => 0 for u in unique_letters)
    for u in unique_letters
        for pair in keys(counts)
            if u in pair
                letter_counts[u] += counts[pair]*count(==(u), pair)
            end
        end
    end
    for l in keys(letter_counts)
        if l in temp_first_last
            letter_counts[l] += 1
        end
        letter_counts[l] /= 2
    end
    letter_counts
end

function part_12(input, steps)
    counts, pr, ul, tfl = parser(input)
    for _ in 1:steps
        counts = poly_expand!(counts, pr)
    end
    c = count_letters(counts, ul, tfl)
    maximum(values(c)) - minimum(values(c))
end
@show part_12(input, 10)
@show part_12(input, 40)

@testset "day14" begin
    @test part_12(testinput, 10) == 1588
    @test part_12(testinput, 40) == 2188189693529
end
retest()
