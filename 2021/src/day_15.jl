# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/15

using ReTest
using DataStructures

const CI = CartesianIndex
const CIs = CartesianIndices

input = readlines("2021/data/day_15.txt")
testinput = readlines("2021/data/day_15_test.txt")

function parser(input)
    permutedims(reduce(hcat, [parse.(Int, collect(line)) for line in input]))
end

function get_adj(CIgrid, pos)
    adjs = (CI(-1,0), CI(1,0), CI(0,-1), CI(0,1))
    [pos + adj for adj in adjs if pos+adj in CIgrid]
end

#######################
## Dijkstra's algorithm
#######################

mutable struct vertex
    edges::Vector{CI}
    risk::Int
    cost::Union{Int, Float64}
end
vertex(edges, risk) = vertex(edges, risk, Inf)

function create_graph(grid)
    CIgrid = CIs(grid)
    graph = Dict{CI, vertex}()
    for ci in CIgrid
        v = vertex(get_adj(CIgrid, ci), grid[ci])
        push!(graph, ci => v)
    end
    graph[CI(1,1)].cost = 0
    graph[CI(1,1)].risk = 0
    graph
end

function create_vertex_prio_queue(graph)
    pq = PriorityQueue{CI, Union{Int, Float64}}() 
    for v in keys(graph)
        enqueue!(pq, v => graph[v].cost)
    end
    pq
end


function part_1(input)
    graph = input |> parser |> create_graph
    pq = create_vertex_prio_queue(graph)
    finish = maximum(keys(graph))
    while !isempty(pq)
        u = dequeue!(pq)
        # display(u)
        # display(graph[u])
        u == finish && return graph[u].cost
        for e in graph[u].edges
            !in(e, keys(pq)) && continue
            cost_alt = graph[u].cost + graph[e].risk
            if cost_alt < pq[e]
                pq[e] = cost_alt
                graph[e].cost = cost_alt
            end
        end
    end
end
@show part_1(input)

function bigger_cave(grid)
    downward = reduce(vcat, grid.+i for i in 0:4)
    full = reduce(hcat, downward.+i for i in 0:4)
    mod1.(full, 9)
end

function part_2(input)
    grid = bigger_cave(parser(input))
    graph = create_graph(grid)
    pq = create_vertex_prio_queue(graph)
    finish = maximum(keys(graph))
    while !isempty(pq)
        u = dequeue!(pq)
        # display(u)
        # display(graph[u])
        u == finish && return graph[u].cost
        for e in graph[u].edges
            !in(e, keys(pq)) && continue
            cost_alt = graph[u].cost + graph[e].risk
            if cost_alt < pq[e]
                pq[e] = cost_alt
                graph[e].cost = cost_alt
            end
        end
    end
end
@show part_2(input)

@testset "day15" begin
    @test part_1(testinput) == 40
    @test part_2(testinput) == 315
end
retest()

# !!Old version!!
#
# function get_adj_forward(grid, pos, visited)
#     CIgrid = CIs(grid)
#     adjs = (CI(1,0), CI(0,1))
#     [pos + adj for adj in adjs if ((pos+adj in CIgrid) && !(pos+adj in visited))]
# end

# cost(path) = sum(path)

# isfinish(grid, pos) = pos == CI(size(grid))

# all_paths_forward(steps) = unique(combinations(repeat([CI(1,0), CI(0,1)], steps), steps))

# function smarter_all_paths(steps) # bad, wants to go back to already visited places
#     unique(combinations(append!(repeat([CI(1,0), CI(0,1)], steps), repeat([CI(-1,0), CI(0,-1)], floor(Int, steps/6))), steps))
# end

# function find_path(grid, forward_looking_steps)
#     visited = [CI(1,1)]
#     path_values = Vector{Int}()
#     paths = all_paths_forward(forward_looking_steps)
#     # paths = smarter_all_paths(forward_looking_steps)
#     CIgrid = CIs(grid)
#     while !isfinish(grid, last(visited))
#         minimum_risk = 9999
#         minimum_risk_steps = Vector{CI}()
#         @multibreak for path in paths
#             tmp_risk = 0
#             tmp_pos = last(visited)
#             for step in path
#                 tmp_pos += step
#                 isfinish(grid, tmp_pos) && break
#                 if !(tmp_pos in CIgrid)
#                     break; continue
#                 end
#                 tmp_risk += grid[tmp_pos]
#                 if tmp_risk > minimum_risk
#                     break; continue
#                 end
#             end
#             if tmp_risk == minimum_risk
#                 push!(minimum_risk_steps, first(path))
#             elseif tmp_risk < minimum_risk
#                 minimum_risk = tmp_risk
#                 minimum_risk_steps = [first(path)]
#             end
#         end
        
#         push!(visited, last(visited) + first(minimum_risk_steps)) # Siia targem valik
#         push!(path_values, grid[last(visited)])
#     end
#     cost(path_values), visited
# end


# function part_1(input, forward_looking_steps)
#     grid = parser(input)
#     c, v = find_path(grid, forward_looking_steps)
#     c
# end
# @show part_1(input, 10)