# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/16

using ReTest

input = read("2021/data/day_16.txt", String)
testinput = readlines("2021/data/day_16_test.txt")

hex2bin(c::Char) = reverse(digits(parse(Int, c, base=16), base=2, pad=4))
hex2bin(cs::Vector{Char}) = reduce(vcat, hex2bin.(cs))
hex2bin(s::String) = hex2bin(collect(s))
bin2dec(v::Vector{Int}) = foldl((x,y) -> 2*x+y, v)

function readbincode!(len::Int, c::Base.RefValue{Int}, bincode::Vector{Int}, raw=false)
    if raw
        value = bincode[c[]:c[]+len-1]
    else
        value = bin2dec(bincode[c[]:c[]+len-1])
    end
    c[] += len
    value
end

gt(x) = x[1]>x[2]
lt(x) = x[1]<x[2]
eq(x) = x[1]==x[2]

test_type2op = Dict(0=>sum,
                    1=>prod,
                    2=>minimum,
                    3=>maximum,
                    5=>gt,
                    6=>lt,
                    7=>eq,
                    )

function parsepacket!(c::Base.RefValue{Int}, bincode::Vector{Int})
    startc = c[]
    versionsum = 0
    versionsum += readbincode!(3, c, bincode)
    type = readbincode!(3, c, bincode)
    subpacknum = 0
    subpacklen = 0
    values = Vector{Int}()
    if type == 4
        while readbincode!(1, c, bincode) == 1
            append!(values, readbincode!(4, c, bincode, true))
        end
        append!(values, readbincode!(4, c, bincode, true))
        value = bin2dec(values)
        # @show value, values
    else
        if readbincode!(1, c, bincode) == 1
            subpacknum = readbincode!(11, c, bincode)
            for _ in 1:subpacknum
                v, _, value = parsepacket!(c, bincode)
                versionsum += v
                append!(values, value)
            end
        else
            subpacklen = readbincode!(15, c, bincode)
            while subpacklen>0
                v, len, value = parsepacket!(c, bincode)
                subpacklen -= len
                versionsum += v
                append!(values, value)
            end
        end
        value = test_type2op[type](values)
        # @show type, values, value
    end
    endc = c[]
    versionsum, endc-startc, value
end

function parseBITS(bincode::Vector{Int}, retvalue)
    c = Ref(1)
    parsepacket!(c, bincode)[retvalue]
end

parseBITS(hexcode::String, retvalue) = parseBITS(hex2bin(hexcode), retvalue)

function part_1(input)
    parseBITS(input, 1)
end
@show part_1(input)

function part_2(input)
    parseBITS(input, 3)
end
@show part_2(input)

@testset "day16" begin
    @test part_1(testinput[1]) == 6
    @test part_1(testinput[2]) == 9
    @test part_1(testinput[3]) == 14
    @test part_1(testinput[4]) == 16
    @test part_1(testinput[5]) == 12
    @test part_1(testinput[6]) == 23
    @test part_1(testinput[7]) == 31
    @test part_2("C200B40A82") == 3
    @test part_2("04005AC33890") == 54
    @test part_2("880086C3E88112") == 7
    @test part_2("CE00C43D881120") == 9
    @test part_2("D8005AC2A8F0") == 1
    @test part_2("F600BC2D8F") == 0
    @test part_2("9C005AC2F8F0") == 0
    @test part_2("9C0141080250320F1802104A08") == 1
end
retest()