# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/17

using ReTest

input = read("2021/data/day_17.txt", String)
testinput = read("2021/data/day_17_test.txt", String)

parse_target_area(input) = parse.(Int, match(r"(-?\d+)..(-?\d+).*=(-?\d+)..(-?\d+)", input).captures)

function pos(vx,vy, step)
    y = sum(vy - t for t in 0:step-1)
    if vx > step
        x = sum(vx-t for t in 0:step-1)
    else
        x = sum(i for i in 1:vx)
    end
    x,y
end

function maxv(area)
    maxvy = abs(area[3])-1
    minvy = area[3]
    maxvx = area[2]
    maxvx, minvy, maxvy
end

intarget(x, y, area) = (area[1] <= x <= area[2]) && (area[3] <= y <= area[4]) 

function part_1(input)
    _, _, ymin, _ = parse_target_area(input)
    floor(Int, ymin*(ymin+1)/2)
end
@show part_1(input)


function part_2(input)
    vxvy = Set{Tuple{Int,Int}}()
    area = parse_target_area(input)
    maxvx, minvy, maxvy = maxv(area)
    for vx in 1:maxvx
        for vy in minvy:maxvy
            for t in 1:1000
                x,y = pos(vx,vy,t)
                if (x>area[2]) && (y<area[3])
                    break
                end
                if intarget(x,y, area)
                    # @show vx, vy, x, y, t
                    push!(vxvy, (vx, vy))
                end
            end
        end
    end
    length(vxvy)
end
@show part_2(input)


@testset "day17" begin
    @test part_1(testinput) == nothing
    @test part_2(testinput) == nothing
end
retest()