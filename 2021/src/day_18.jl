# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/18

using ReTest
import Base:+

input = readlines("2021/data/day_18.txt")
# testinput = readlines("2021/data/day_18_test.txt")

abstract type SnailNumber end

mutable struct SnailPair <: SnailNumber
    left::Union{SnailPair, Int}
    right::Union{SnailPair, Int}
    level::Int
end

function +(x::T, y::T) where T<:SnailNumber
    SnailPair(x, y, 1)
end

function reduction(x::SnailPair, level=1)



function part_1(input)
    nothing
end
# @show part_1(input)

function part_2(input)
    nothing
end
# @show part_2(input)

@testset "day18" begin
    @test part_1(testinput) == nothing
    @test part_2(testinput) == nothing
end
retest()