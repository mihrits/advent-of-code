# Moorits Mihkel Muru
# https://adventofcode.com/2021/day/2
using AdventOfCode
using ReTest
using Match

input = readlines("2021/data/day_2.txt")

function part_1(input)
    depth, pos = 0, 0
    for command in input
        value = parse(Int, split(command)[end])

        @match command[1] begin
            'd' => (depth += value)
            'u' => (depth -= value)
            'f' => (pos += value)
        end
    end
    depth * pos
end
@info part_1(input)

function part_2(input)
    depth, pos, aim = 0, 0, 0
    for command in input
        value = parse(Int, split(command)[end])

        @match command[1] begin
            'd' => (aim += value)
            'u' => (aim -= value)
            'f' => (pos += value, depth += aim * value)
        end
    end
    depth * pos
end
@info part_2(input)

@testset "day02" begin
    testinput = readlines(IOBuffer(
        """
        forward 5
        down 5
        forward 8
        up 3
        down 8
        forward 2
        """
    ))
    @test part_1(testinput) == 150
    @test part_2(testinput) == 900
end
