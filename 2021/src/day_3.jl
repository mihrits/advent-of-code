# Moorits Mihkel Muru
# https://adventofcode.com/2021/day/3
using AdventOfCode
using ReTest

input = readlines("2021/data/day_3.txt")

function part_1(input)
    # Collect input into a matrix of characters
    char_matrix = reduce(vcat, permutedims.(collect.(input)))
    # Find the most common bit for each column
    γ = count(==('1'), char_matrix, dims=1) .> floor(Int, length(input)/2)
    ε = .~γ # bitwise not
    prod(x -> parse(Int, join(convert.(Int, x)), base=2), (γ, ε))
end
@info part_1(input)

function part_2(input)
    char_matrix = reduce(vcat, permutedims.(collect.(input)))
    oxy = char_matrix
    for idx in 1:size(char_matrix, 2)
	if size(oxy, 1) == 1
	    break
	end
	common_value = count(==('1'), oxy[:, idx]) >= size(oxy, 1)/2 
	oxy = oxy[oxy[:, idx] .== ('0' + common_value), :]
    end

    co2 = char_matrix
    for idx in 1:size(char_matrix, 2)
	if size(co2, 1) == 1
	    break
	end
	common_value = count(==('1'), co2[:, idx]) < size(co2, 1)/2 
	co2 = co2[co2[:, idx] .== ('0' + common_value), :]
    end

    prod(x -> parse(Int, join(parse.(Int, x)), base=2), (oxy, co2))
end
@info part_2(input)

@testset "day03" begin
    testinput = readlines(IOBuffer(
	"""
	00100
	11110
	10110
	10111
	10101
	01111
	00111
	11100
	10000
	11001
	00010
	01010
	"""
    ))
    @test part_1(testinput) = 198
    @test part_2(testinput) = 230
end
