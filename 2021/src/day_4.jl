# Moorits Mihkel Muru
# https://adventofcode.com/2021/day/4

using ReTest

input = read("2021/data/day_4.txt", String)
testinput = read("2021/data/day_4_test.txt", String)

function parse_input(input)
    splitinput = split(input, "\n\n")

    draws = parse.(Int, split(splitinput[1], ","))

    boards_str = split.(splitinput[2:end], "\n", keepempty=false)
    boards = [parse.(Int, reduce(hcat, split.(board))) for board in boards_str]

    draws, boards
end

function has_won(board)
    w = fill(-1, 5) # win condition five -1s in a row/col
    any(==(w), eachrow(board)) || any(==(w), eachcol(board)) # compare against each row/col
end

function part_1(input)
    draws, boards = parse_input(input)

    for draw in draws
        for board in boards
            if draw ∈ board
                board[findfirst(board .== draw)] = -1
                if has_won(board)
                    return sum(filter(>(0), board)) * draw
                end
            end
        end
    end
    
    prtinln("No winning boards!")
end
@show part_1(input)

function part_2(input)
    draws, boards = parse_input(input)

    wincount = 0
    boardscount = length(boards)

    for draw in draws
        for board in boards
            if draw ∈ board
                board[findfirst(board .== draw)] = -1
                if has_won(board)
                    wincount += 1
                    if wincount == boardscount
                        return sum(filter(>(0), board)) * draw
                    end
                    fill!(board, -1)
                end
            end
        end
    end

    println("How did you reach here?")
end
@show part_2(input)

@testset "day04" begin
    @test part_1(testinput) == 4512
    @test part_2(testinput) == 1924
end