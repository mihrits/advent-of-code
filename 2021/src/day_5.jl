# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/5

using ReTest

input = readlines("2021/data/day_5.txt")
testinput = readlines("2021/data/day_5_test.txt")

function parse_crds(input)
    collect(parse.(Int, split(replace(line, " -> " => ","), ",")) for line in input)
end

function interpolate(crds; diagonal_lines=false)
    vents = Vector{Tuple{Int, Int}}()
    for crd in crds
        if diagonal_lines || is_not_diagonal(crd)
            x₀ = crd[1]
            y₀ = crd[2]
            Δx = crd[3]-crd[1]
            Δy = crd[4]-crd[2]
            append!(vents, [(x₀+i*sign(Δx), y₀+i*sign(Δy)) for i in 0:max(abs(Δx), abs(Δy))])
        end
    end
    vents
end

function is_not_diagonal(crd)
    (crd[1] == crd[3]) || (crd[2] == crd[4])
end

function part_1(input)
    vents = interpolate(parse_crds(input))
    count(>=(2), count(==(crd), vents) for crd in unique(vents))
end
@show part_1(input)

function part_2(input)
    vents = interpolate(parse_crds(input); diagonal_lines=true)
    count(>=(2), count(==(crd), vents) for crd in unique(vents))
end
@show part_2(input)

@testset "day05" begin
    @test part_1(testinput) == 5
    @test part_2(testinput) == 12
end