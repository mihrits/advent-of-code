# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/6

using ReTest
using DelimitedFiles
using ProgressMeter

input = vec(readdlm("2021/data/day_6.txt", ',', Int))
testinput = vec(readdlm("2021/data/day_6_test.txt", ',', Int))

function evolve(population)
    new_population = population .- 1
    new_fish_count = count(==(0), new_population)
    append!(new_population, fill(9, new_fish_count))
    replace!(new_population, -1 => 6)
end

function part_1(population, day)
    p = Progress(day-1)
    while day > 1
        population = evolve(population)
        day -= 1
        next!(p)
    end
    length(population)
end
@show part_1(input, 80)


function part_2(input, days)
    adults = [count(==(i), input) for i in 0:6] # adults get offspring every 7 days
    newborns = zeros(Int, 7) # newborns are newborns for 2 days
    teenagers = zeros(Int, 7) # teenagers get ready to join adults, it takes 7 days
    for day in 1:days-1
        d = mod(day, 7) + 1
        adults[d] += teenagers[d] # teenagers join adults
        teenagers[d] = newborns[mod(d-3, 7) + 1] # new teenagers from newborns
        newborns[mod(d-3, 7) + 1] = 0 # need to zero to find result
        newborns[d] = adults[d]
    end
    sum(adults) + sum(teenagers) + sum(newborns)
end
@show part_2(input, 256)

@testset "day06" begin
    @test part_1(testinput, 18) == 26
    @test part_1(testinput, 80) == 5934
    @test part_2(testinput, 1) == 5
    @test part_2(testinput, 18) == 26
    @test part_2(testinput, 80) == 5934
    @test part_2(testinput, 256) == 26984457539
end