# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/XX

using ReTest
using DelimitedFiles
using Statistics

input = vec(readdlm("2021/data/day_7.txt", ',', Int))
testinput = vec(readdlm("2021/data/day_7_test.txt", ',', Int))

function part_1(crabs)
    sum(abs.(crabs .- median(crabs)))
end
@show part_1(input)

function part_2(crabs)
    fuel_cost(distance) = convert(Int, (1+distance) * distance / 2)
    ps = (sum(fuel_cost.(abs.(crabs .- p))) for p in minimum(crabs):maximum(crabs))
    minimum(ps)
end
@show part_2(input)

@testset "day07" begin
    @test part_1(testinput) == 37
    @test part_2(testinput) == 168
end
