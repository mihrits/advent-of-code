# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/8

using ReTest
using Match

input = readlines("2021/data/day_8.txt")
testinput = readlines("2021/data/day_8_test.txt")

function part_1(input)
    str_lengths = zeros(Int, 6)
    for line in input
        d = length.(split(split(line, "|")[2]))
        str_lengths += [count(==(i), d) for i in 2:7]
    end
    str_lengths[1] + str_lengths[2] + str_lengths[3] + str_lengths[6] # sum of 1s, 7s, 4s and 8s
end
@show part_1(input)

function decoder(uniques)
    to_int = Dict{Set{Char}, Int}()
    from_int = Dict{Int, Set{Char}}()
    lengths = length.(uniques)

    # takes only strings with length len, converts them to sets and filters by filt
    strX(len, filt) = only(filter(filt, Set.(uniques[lengths .== len])))

    function push_strX!(value, len, filt)
        push!(to_int, strX(len, filt) => value)
        push!(from_int, value => strX(len, filt))
    end
    push_strX!(value, len) = push_strX!(value, len, s->true)

    push_strX!(1, 2)
    push_strX!(4, 4)
    push_strX!(7, 3)
    push_strX!(8, 7)
    push_strX!(6, 6, s -> from_int[1] ⊈ s)
    push_strX!(9, 6, s -> from_int[4] ⊆ s)
    push_strX!(3, 5, s -> from_int[1] ⊆ s)
    push_strX!(5, 5, s -> s ⊆ from_int[6])
    push_strX!(2, 5, s -> s ⊈ from_int[9])
    push_strX!(0, 6, s -> from_int[5] ⊈ s)

    to_int
end

function part_2(input)
    sum = 0
    for line in input
        uniques, digits = split.(split(line, "|"))
        dec = decoder(uniques)
        sum += parse(Int, join([ dec[x] for x in Set.(digits)]))
    end
    sum
end
@show part_2(input)

@testset "day8" begin
    shorttest = ["acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"]
    @test part_1(testinput) == 26
    @test part_2(shorttest) == 5353
    @test part_2(testinput) == 61229
end