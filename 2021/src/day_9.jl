# Moorits Mihkel Muru
# htts://adventofcode.com/2021/day/9

using ReTest

const CI = CartesianIndex

input = readlines("2021/data/day_9.txt")
testinput = readlines("2021/data/day_9_test.txt")

parse_data(input) = parse.(Int, reduce(hcat, collect.(input)))

function get_adjacent_values(data, crd)
    adjacent_values = Vector{Int}()
    ΔCIs =(CI(1,0), CI(0,1), CI(-1,0), CI(0,-1))
    for Δ in ΔCIs
        v::Int8 = 9
        try v = data[crd + Δ] catch e end
        append!(adjacent_values, v)
    end
    adjacent_values
end

is_minima(curr_value, adjacent) = all(adjacent .> curr_value) 

function part_1(input)
    data = parse_data(input)
    risk = 0
    for crd in CartesianIndices(size(data))
        if is_minima(data[crd], get_adjacent_values(data, crd))
            risk += data[crd] + 1
        end
    end
    risk
end
@show part_1(input)

function get_basin(data, minimacrd)
    adjacent_crds = Set{CartesianIndex{2}}()
    basin_crds = Set{CartesianIndex{2}}()
    push!(adjacent_crds, minimacrd)
    ΔCIs =(CI(1,0), CI(0,1), CI(-1,0), CI(0,-1))
    while !isempty(adjacent_crds)
        crd = pop!(adjacent_crds)
        push!(basin_crds, crd)
        for Δ in ΔCIs
            newcrd = crd + Δ
            try
                if (newcrd ∉ basin_crds) && (data[newcrd] < 9)
                    push!(adjacent_crds, newcrd)
                end
            catch
            end
        end
    end
    basin_crds    
end

function get_minima_crds(data)
    minima_crds = Set{CartesianIndex{2}}()
    for crd in CartesianIndices(size(data))
        if is_minima(data[crd], get_adjacent_values(data, crd.I...))
            push!(minima_crds, crd)
        end
    end
    minima_crds
end

function part_2(input)
    data = parse_data(input)
    minima_crds = get_minima_crds(data)
    basin_sizes = Vector{Int}()
    for minima in minima_crds
        push!(basin_sizes, length(get_basin(data, minima)))
    end
    prod(sort!(basin_sizes, rev=true)[1:3])
end
@show part_2(input)

@testset "day9" begin
    @test part_1(testinput) == 15
    @test part_2(testinput) == 1134
end