# Moorits Mihkel Muru
# htts://adventofcode.com/2022/day/1

using ReTest

input = read("2022/data/day_1.txt", String)


function part_1(input)
    data = split.(split(input, "\n\n"), "\n")
    maxcal = 0
    for elf in data
        if elf == [""] continue end
        elfcal = sum(parse.(Int, elf))
        if elfcal > maxcal maxcal = elfcal end
    end
    maxcal
end
@show part_1(input)

function part_2(input)
    data = split.(split(input, "\n\n"), "\n")
    cals = Int[]
    for elf in data
        if elf == [""] continue end
        push!(cals, sum(parse.(Int, elf)))
    end
    sort!(cals, rev=true)
    sum(cals[1:3])
end
@show part_2(input)

testinput = """1000
2000
3000

4000

5000
6000

7000
8000
9000

10000"""

@testset "day1" begin
    @test part_1(testinput) == 24000
    @test part_2(testinput) == 45000
end
retest()
