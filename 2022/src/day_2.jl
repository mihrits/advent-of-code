# Moorits Mihkel Muru
# htts://adventofcode.com/2022/day/2

using ReTest

input = readlines("2022/data/day_2.txt")[1:end-1]
testinput = split("""A Y
B X
C Z""", "\n")

shapescores(game) = (last(game) - 'X') + 1

function outcomes(game)
    o = ((last(game) - 'X') - (first(game) - 'A'))
    if o == 0
        return 3
    elseif o in [-1, 2]
        return 0
    elseif o in [1, -2]
        return 6
    end
end

function part_1(input)
    score = 0
    for game in input
        score += outcomes(game) + shapescores(game)
    end
    score
end
@show part_1(input)

outcomes_2(game) = (last(game) - 'X') * 3
function shapescores_2(game)
    o = (first(game) - 'A')
    if outcomes_2(game) == 0
        return mod(o+2,3) + 1
    elseif outcomes_2(game) == 3
        return o+1
    elseif outcomes_2(game) == 6
        return mod1(o+2,3)
    end
end

function part_2(input)
    score = 0
    for game in input
        score += outcomes_2(game) + shapescores_2(game)
    end
    score
end
# @show part_2(input)

@testset "day2" begin
    @test part_1(testinput) == 15
    @test part_2(testinput) == nothing
end
retest()
