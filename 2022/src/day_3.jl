# Moorits Mihkel Muru
# htts://adventofcode.com/2022/day/3

using ReTest

input = readlines("2022/data/day_3.txt")
testinput = readlines("2022/data/day_3_test.txt")

function score(letter::Char)
    if isuppercase(letter)
        s = letter - 'A' + 26
    else
        s = letter - 'a'
    end
    s + 1
end

function part_1(input)
    s = 0
    for line in input
        l = length(line) ÷ 2
        c1 = collect(line[1:l])
        c2 = collect(line[l+1:end])
        s += score(only(∩(c1, c2)))
    end
    s
end
@show part_1(input)

function part_2(input)
    s = 0
    for gid in 1:length(input)÷3
        gstart = (gid-1)*3+1
        s += score(only(reduce(∩, input[gstart:gstart+2])))
    end
    s
end
@show part_2(input)

@testset "day3" begin
    @test part_1(testinput) == 157
    @test part_2(testinput) == 70
end
retest()
