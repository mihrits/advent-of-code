# Moorits Mihkel Muru
# htts://adventofcode.com/2022/day/4

using ReTest

input = readlines("2022/data/day_4.txt")
testinput = readlines("2022/data/day_4_test.txt")

function part_1(input)
    s = 0
    for line in input
        m = match(r"(\d+)-(\d+),(\d+)-(\d+)", line)
        r = parse.(Int, m.captures)
        r1 = r[1]:r[2]
        r2 = r[3]:r[4]
        s += issubset(r1,r2) || issubset(r2,r1)
    end
    s
end
# @show part_1(input)

function part_2(input)
    s = 0
    for line in input
        m = match(r"(\d+)-(\d+),(\d+)-(\d+)", line)
        r = parse.(Int, m.captures)
        r1 = r[1]:r[2]
        r2 = r[3]:r[4]
        if length(∩(r1,r2)) > 0
            s += 1
        end
    end
    s
end
# @show part_2(input)

@testset "day4" begin
    @test part_1(testinput) == nothing
    @test part_2(testinput) == nothing
end
retest()
