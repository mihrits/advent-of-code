# Moorits Mihkel Muru
# htts://adventofcode.com/2022/day/5

using ReTest

input = readlines("2022/data/day_5.txt")
testinput = readlines("2022/data/day_5_test.txt")

function initpos(input)
    fin_idx = findfirst(startswith(" 1"), input)
    nstacks = maximum(parse.(Int,split(input[fin_idx])))
    stacks = [Vector{Char}() for _ in 1:nstacks]
    schema = input[1:fin_idx-1]
    for level in schema
        crates = level[2:4:end]
        for i in 1:length(crates)
            if crates[i] != ' '
                pushfirst!(stacks[i], crates[i])
            end
        end
    end
    stacks, fin_idx
end

function part_1(input)
    stacks, idx = initpos(input)
    for instruction in input[idx+2:end]
        cnt, from, to = parse.(Int, match(r"(\d+).*(\d+).*(\d+)", instruction).captures)
        for i in 1:cnt
            push!(stacks[to], pop!(stacks[from]))
        end
    end
    prod(last(s) for s in stacks)
end
# @show part_1(input)

function part_2(input)
    stacks, idx = initpos(input)
    for instruction in input[idx+2:end]
        cnt, from, to = parse.(Int, match(r"(\d+).*(\d+).*(\d+)", instruction).captures)
        move = Vector{Char}()
        for _ in 1:cnt
            pushfirst!(move, pop!(stacks[from]))
        end
        append!(stacks[to], move)
    end
    prod(last(s) for s in stacks)
end
# @show part_2(input)

@testset "day5" begin
    @test part_1(testinput) == nothing
    @test part_2(testinput) == nothing
end
retest()
