# https://adventofcode.com/2022/day/6

using ReTest

input = only(readlines("2022/data/day_6.txt"))
test1 = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"
test2 = "bvwbjplbgvbhsrlpgdmjqwftvncz"
test3 = "nppdvjthqldpwncqszvftbrmjlhg"
test4 = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
test5 = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"

function part_1(input)
    for i in 1:length(input)-4
        if length(Set(input[i:i+3])) == 4
            return i+3
        end
    end
    println("Nohting found!")
end
@info part_1(input)

function part_2(input)
    for i in 1:length(input)-14
        if length(Set(input[i:i+13])) == 14
            return i+13
        end
    end
end
@info part_2(input)

@testset "day6" begin
    @test part_1(test1) == 7
    @test part_1(test2) == 5
    @test part_1(test3) == 6
    @test part_1(test4) == 10
    @test part_1(test5) == 11
    @test part_2(test1) == 19
    @test part_2(test2) == 23
    @test part_2(test3) == 23
    @test part_2(test4) == 29
    @test part_2(test5) == 26
end
