# https://adventofcode.com/2023/day/1

input = readlines("2023/data/day_1.txt")
testinput = readlines("2023/data/day_1_test.txt")
testinput2 = readlines("2023/data/day_1_test2.txt")

function part_1(input)
    s = 0
    for line in input
        numbers = filter(isnumeric, collect(line))
        s += parse(Int, first(numbers) * last(numbers))
    end
    s
end
@info part_1(input)

function part_2(input)
    s = 0
    numbernames = ("one", "two", "three", "four", "five", "six", "seven", "eight", "nine")
    names2numbers = Dict(
        "one" => 1,
        "two" => 2,
        "three" => 3,
        "four" => 4,
        "five" => 5,
        "six" => 6,
        "seven" => 7,
        "eight" => 8,
        "nine" => 9,
        '1' => 1,
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
    )
    for line in input
        firstindex = 99
        firstvalue = 0
        lastindex = 0
        lastvalue = 0
        for number in (collect('1':'9')..., numbernames...)
            occurances = findall(number, line)
            if any(first.(occurances) .< firstindex)
                firstvalue = names2numbers[number]
                firstindex = minimum(first.(occurances))
            end
            if any(first.(occurances) .> lastindex)
                lastvalue = names2numbers[number]
                lastindex = maximum(first.(occurances))
            end
        end
        s += parse(Int, string(firstvalue) * string(lastvalue))
    end
    s
end
@info part_2(input)


