# https://adventofcode.com/2023/day/10

using Match

input = readlines("2023/data/day_10.txt")
testinput1 = readlines("2023/data/day_10_test1.txt")
testinput2 = readlines("2023/data/day_10_test2.txt")
testinput3 = readlines("2023/data/day_10_test3.txt")
testinput4 = readlines("2023/data/day_10_test4.txt")
testinput5 = readlines("2023/data/day_10_test5.txt")
testinput6 = readlines("2023/data/day_10_test6.txt")

function parse_input(input)
    permutedims(stack(collect.(input)))
end

function get_connection(pipe::Char, lastmove::CartesianIndex)
    move = @match pipe begin
        '-' => lastmove
        '|' => lastmove
        'F' => reverse(Tuple(lastmove)).* (-1)
        'J' => reverse(Tuple(lastmove)).* (-1)
        '7' => reverse(Tuple(lastmove))
        'L' => reverse(Tuple(lastmove))
    end
    CartesianIndex(move)
end

function get_startingmoves(start::CartesianIndex, pipes)
    bounds = CartesianIndices(pipes)
    startingmoves = []
    for move in CartesianIndex.(((0,1), (1,0), (0,-1), (-1,0)))
        if start+move in bounds
            if move[2] == 0
                if move[1] == 1
                    if pipes[start+move] in ('|', 'J', 'L')
                        push!(startingmoves, move)
                    end
                elseif move[1] == -1
                    if pipes[start+move] in ('|', '7', 'F')
                        push!(startingmoves, move)
                    end
                end
            elseif move[1] == 0
                if move[2] == 1
                    if pipes[start+move] in ('-', 'J', '7')
                        push!(startingmoves, move)
                    end
                elseif move[2] == -1
                    if pipes[start+move] in ('-', 'L', 'F')
                        push!(startingmoves, move)
                    end
                end
            end
        end
    end
    startingmoves
end

function part_1(input)
    pipes = parse_input(input)
    start = findfirst(==('S'), pipes)
    lastmove1, lastmove2 = get_startingmoves(start, pipes)
    pos1 = start + lastmove1
    pos2 = start + lastmove2
    steps = 0
    while pos1 != pos2
        steps += 1
        lastmove1 = get_connection(pipes[pos1], lastmove1)
        pos1 += lastmove1
        lastmove2 = get_connection(pipes[pos2], lastmove2)
        pos2 += lastmove2
    end
    steps + 1
end
@info part_1(input)

function part_2(input)
    pipes = parse_input(input)
    pipeline = [findfirst(==('S'), pipes)]
    moves = [get_startingmoves(pipeline[1], pipes)[1]]
    turns = [0,0] # Count [left, right] turns
    while last(pipeline) + last(moves) != first(pipeline)
        curr = last(pipeline) + last(moves)
        push!(pipeline, curr)
        move = get_connection(pipes[curr], last(moves))
        push!(moves, move)
        currsymbol = pipes[curr]
        if currsymbol == 'F'
            if move[1] == 0 # If we continue horizontally
                turns[2] += 1 # Add 1 to right turns
            else
                turns[1] += 1
            end
        elseif currsymbol == 'J'
            if move[1] == 0
                turns[2] += 1
            else
                turns[1] += 1
            end
        elseif currsymbol == '7'
            if move[1] == 0
                turns[1] += 1
            else
                turns[2] += 1
            end
        elseif currsymbol == 'L'
            if move[1] == 0
                turns[1] += 1
            else
                turns[2] += 1
            end
        end
    end
    right_inside = sign(only(diff(turns)))
    insidetiles = CartesianIndex[]
    while_end_cond(tile) = (tile in pipeline)# || (tile in insidetiles)
    for (pipe, move) in zip(pipeline, moves)
        curr = pipes[pipe]
        tile = pipe
        if curr == '|'
            while true
                tile += right_inside * move[1] * CartesianIndex(0,-1)
                while_end_cond(tile) && break
                push!(insidetiles, tile)
            end
        elseif curr == '-'
            while true
                tile += right_inside * move[2] * CartesianIndex(1,0)
                while_end_cond(tile) && break
                push!(insidetiles, tile)
            end
        elseif curr == 'F'
            if move == CartesianIndex(1,0)
                if right_inside == 1
                    while true
                        tile += CartesianIndex(0,-1)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                    tile = pipe
                    while true
                        tile += CartesianIndex(-1,0)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                end
            elseif move == CartesianIndex(0,1)
                if right_inside == -1
                    while true
                        tile += CartesianIndex(0,-1)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                    tile = pipe
                    while true
                        tile += CartesianIndex(-1,0)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                end
            end
        elseif curr == 'J'
            if move == CartesianIndex(-1,0)
                if right_inside == 1
                    while true
                        tile += CartesianIndex(0,1)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                    tile = pipe
                    while true
                        tile += CartesianIndex(1,0)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                end
            elseif move == CartesianIndex(0,-1)
                if right_inside == -1
                    while true
                        tile += CartesianIndex(0,1)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                    tile = pipe
                    while true
                        tile += CartesianIndex(1,0)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                end
            end
        elseif curr == '7'
            if move == CartesianIndex(0,-1)
                if right_inside == 1
                    while true
                        tile += CartesianIndex(0,1)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                    tile = pipe
                    while true
                        tile += CartesianIndex(-1,0)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                end
            elseif move == CartesianIndex(1,0)
                if right_inside == -1
                    while true
                        tile += CartesianIndex(0,1)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                    tile = pipe
                    while true
                        tile += CartesianIndex(-1,0)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                end
            end
        elseif curr == 'L'
            if move == CartesianIndex(0,1)
                if right_inside == 1
                    while true
                        tile += CartesianIndex(0,-1)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                    tile = pipe
                    while true
                        tile += CartesianIndex(1,0)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                end
            elseif move == CartesianIndex(-1,0)
                if right_inside == -1
                    while true
                        tile += CartesianIndex(0,-1)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                    tile = pipe
                    while true
                        tile += CartesianIndex(1,0)
                        while_end_cond(tile) && break
                        push!(insidetiles, tile)
                    end
                end
            end
        end
    end
    length(Set(insidetiles))
end
@info part_2(input)
