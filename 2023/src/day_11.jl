# https://adventofcode.com/2023/day/11

input = readlines("2023/data/day_11.txt")
testinput = readlines("2023/data/day_11_test.txt")

function parse_input(input)
    permutedims(stack(collect.(input)))
end

function expand(space)
    empty_cols = findall(==(fill('.', size(space,1))), eachcol(space))
    empty_rows = findall(==(fill('.', size(space,2))), eachrow(space))
    expanded_space = copy(space)
    for row in reverse(empty_rows)
        expanded_space = vcat(expanded_space[1:row, :], fill('.', (1, size(expanded_space, 2))), expanded_space[row+1:end, :])
    end
    for col in reverse(empty_cols)
        expanded_space = hcat(expanded_space[:,1:col], fill('.', (size(expanded_space, 1),1)), expanded_space[:,col+1:end])
    end
    expanded_space
end

function part_1(input)
    space = parse_input(input)
    expspace = expand(space)
    gals = findall(==('#'), expspace)
    s = 0
    for gal1 in 1:length(gals)
        for gal2 in gal1:length(gals)
            s += sum(abs, Tuple(gals[gal1] - gals[gal2]))
        end
    end
    s
end
@info part_1(input)

function calc_dist(gal1, gal2, empty_cols, empty_rows, expansion_rate)
    dist = sum(abs, Tuple(gal1-gal2))
    dist += (expansion_rate-1) * sum(r -> min(gal1[1], gal2[1]) < r < max(gal1[1], gal2[1]), empty_rows)
    dist += (expansion_rate-1) * sum(c -> min(gal1[2], gal2[2]) < c < max(gal1[2], gal2[2]), empty_cols)
    dist
end

function part_2(input, expansion_rate=1e6)
    space = parse_input(input)
    empty_cols = findall(==(fill('.', size(space,1))), eachcol(space))
    empty_rows = findall(==(fill('.', size(space,2))), eachrow(space))
    gals = findall(==('#'), space)
    s = 0
    for gal1id in 1:length(gals)
        for gal2id in gal1id:length(gals)
            s += calc_dist(gals[gal1id], gals[gal2id], empty_cols, empty_rows, expansion_rate)
        end
    end
    s
end
@info part_2(input)
