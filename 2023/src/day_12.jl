# https://adventofcode.com/2023/day/12

using Test

input = readlines("2023/data/day_12.txt")
testinput = readlines("2023/data/day_12_test1.txt")

function parse_row(input)
    springs, counts = split(input)
    springs, parse.(Int, split(counts, ","))
end

function fit_springs(row, counts)
    if isempty(counts)
        if !contains(row, '#')
            return 1
        else
            return 0
        end
    end
    c = counts[1]
    if !contains(row, r"[#\?]") || c > length(row) return 0 end
    s = 0
    if (!contains(row[1:c], '.') && (c == length(row) || row[c+1] != '#'))
        s += fit_springs_solved(row[c+2:end], counts[2:end])
    end
    if first(row) in ['.','?']
        s += fit_springs_solved(row[2:end], counts)
    end
    s
end

solved_cases = Dict()

function fit_springs_solved(row, counts)
    get!(solved_cases, (row, counts)) do
        fit_springs(row, counts)
    end
end

function unfold(row, counts)
    row = repeat(row*"?", 5)[1:end-1]
    counts = repeat(counts, 5)
    row, counts
end

function part_1(input)
    sum(fit_springs(parse_row(r)...;debug=false) for r in input)
end
@info part_1(input) # correct answer 7939

function part_2(input)
    sum(fit_springs(unfold(parse_row(r)...)...;debug=false) for r in input)
end
@info part_2(input) # correct answer 850504257483930
