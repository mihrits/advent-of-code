# https://adventofcode.com/2023/day/13

input = read("2023/data/day_13.txt", String)
testinput = read("2023/data/day_13_test.txt", String)
testinput2 = read("2023/data/day_13_test2.txt", String)

function parse_input(input)
    patterns = [map(i -> collect(i.I), findall(==('#'), stack(map(collect, split(p, "\n", keepempty=false))))) for p in split(input, "\n\n")]
end

function flip(pattern, i, dim)
    pmax = maximum(first.(pattern)), maximum(last.(pattern))    
    for p in pattern
        pn = copy(p)
        pn[dim] = 2i + 1 - pn[dim]
        if 1 <= pn[dim] <= pmax[dim]
            if !in(pn, pattern)
                return false
            end
        end
    end
    true
end


function part_1(input)
    patterns = parse_input(input)
    s = 0
    for pattern in patterns
        for dim in [1,2]
            for i in 1:maximum(map(p -> p[dim], pattern))-1
                if flip(pattern, i, dim)
                    s += i * (dim == 1 ? 1 : 100)
                    break
                end
            end
        end
    end
    s
end
@info part_1(input)

function flip2(pattern, i, dim)
    pmax = maximum(first.(pattern)), maximum(last.(pattern))    
    smudge = false
    for p in pattern
        pn = copy(p)
        pn[dim] = 2i + 1 - pn[dim]
        if 1 <= pn[dim] <= pmax[dim]
            if !in(pn, pattern)
                if smudge
                    return false
                end
                smudge = true
            end
        end
    end
    smudge
end


function part_2(input)
    patterns = parse_input(input)
    s = 0
    for pattern in patterns
        for dim in [1,2]
            for i in 1:maximum(map(p -> p[dim], pattern))-1
                if flip2(pattern, i, dim)
                    s += i * (dim == 1 ? 1 : 100)
                    break
                end
            end
        end
    end
    s
end
@info part_2(input)
