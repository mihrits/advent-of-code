# https://adventofcode.com/2023/day/14

input = readlines("2023/data/day_14.txt")
testinput = readlines("2023/data/day_14_test.txt")

function part_1(input)
    rocks = []
    cubes = []
    linemax = length(input)
    for i in 1:linemax
        line = input[i]
        rockcols = findall('O', line)
        append!(cubes, [[i,r] for r in findall('#', line)])
        for col in rockcols
            obstacles = first.(filter(o -> last(o)==col, rocks ∪ cubes))
            row = maximum(obstacles, init=0) + 1
            push!(rocks, [row, col])
        end
    end
    sum(first.(rocks)) do r
        linemax - r + 1
    end
end
@info part_1(input)

function slide(rocks, cubes)
    nothing
end

function part_2(input)
    nothing
end
@info part_2(input)
