# https://adventofcode.com/2023/day/2

input = readlines("2023/data/day_2.txt")
testinput = readlines("2023/data/day_2_test.txt")

function part_1(input)
    answer = 0
    allcubes = Dict("red" => 12, "green" => 13, "blue" => 14)
    for line in input
        currentcubes = Dict("red" => 0, "green" => 0, "blue" => 0)
        ID = parse(Int, only(match(r"Game (\d+):", line).captures))
        hands = split(line, ":")[2]
        for hand in split(hands, ";")
            for cubes in split(hand, ",")
                _, count, color = split(cubes, " ")
                count = parse(Int, count)
                if currentcubes[color] < count currentcubes[color] = count end
            end
        end
        if all([allcubes[color] - currentcubes[color] for color in keys(allcubes)] .>= 0) 
            answer += ID
        end
    end
    answer
end
@info part_1(input)

function part_2(input)
    answer = 0
    for line in input
        currentcubes = Dict("red" => 0, "green" => 0, "blue" => 0)
        ID = parse(Int, only(match(r"Game (\d+):", line).captures))
        hands = split(line, ":")[2]
        for hand in split(hands, ";")
            for cubes in split(hand, ",")
                _, count, color = split(cubes, " ")
                count = parse(Int, count)
                if currentcubes[color] < count currentcubes[color] = count end
            end
        end
        answer += prod(currentcubes[color] for color in keys(currentcubes))
    end
    answer
end
@info part_2(input)
