# https://adventofcode.com/2023/day/3

input = readlines("2023/data/day_3.txt")
testinput = readlines("2023/data/day_3_test.txt")

function get_neighbours(index::CartesianIndex) # Doesn't check for bounds
    [index + CartesianIndex(i,j) for i in -1:1, j in -1:1 if !(i==j==0)]
end

function get_symbol_indices(schematic)
    s = replace(schematic, '.' => 'x') # because I want to use match with regex \W and exclude '.'
    findall(c -> contains(string(c), r"\W"), s)
end

function get_part_numbers(symbol_index, schematic)
    all_num_indices = []
    l = CartesianIndex(0, -1)
    r = CartesianIndex(0, 1)
    neighbours = get_neighbours(symbol_index)
    bounds = CartesianIndices(schematic)
    for n in neighbours
        if !(n in bounds) continue end
        if isdigit(schematic[n])
            numindices = [n]
            m = n + l
            while true
                if !(m in bounds) break end
                if !isdigit(schematic[m]) break end
                push!(numindices, m)
                m += l
            end
            m = n + r
            while true
                if !(m in bounds) break end
                if !isdigit(schematic[m]) break end
                push!(numindices, m)
                m += r
            end
            push!(all_num_indices, sort(numindices))
        end
    end
    parse.(Int, [prod(schematic[indices]) for indices in unique(all_num_indices)])
end

function parse_schematic(input)
    permutedims(stack(map(collect, input)))
end

function part_1(input)
    schematic = parse_schematic(input)
    symbol_indices = get_symbol_indices(schematic)
    s = 0
    for symbol_index in symbol_indices
        s += sum(get_part_numbers(symbol_index, schematic), init = 0)
    end
    s
end
@info part_1(input)

function get_gear_ratio(symbol_index, schematic)
    gear_ratio = 0
    all_num_indices = []
    l = CartesianIndex(0, -1)
    r = CartesianIndex(0, 1)
    neighbours = get_neighbours(symbol_index)
    bounds = CartesianIndices(schematic)
    for n in neighbours
        if !(n in bounds) continue end
        if isdigit(schematic[n])
            numindices = [n]
            m = n + l
            while true
                if !(m in bounds) break end
                if !isdigit(schematic[m]) break end
                push!(numindices, m)
                m += l
            end
            m = n + r
            while true
                if !(m in bounds) break end
                if !isdigit(schematic[m]) break end
                push!(numindices, m)
                m += r
            end
            push!(all_num_indices, sort(numindices))
        end
    end
    gear_numbers = parse.(Int, [prod(schematic[indices]) for indices in unique(all_num_indices)])
    if length(gear_numbers) == 2 gear_ratio = prod(gear_numbers) end
    gear_ratio
end

function get_gear_indices(schematic)
    findall(==('*'), schematic)
end

function part_2(input)
    schematic = parse_schematic(input)
    gear_indices = get_gear_indices(schematic)
    s = 0
    for gear_index in gear_indices
        s += sum(get_gear_ratio(gear_index, schematic), init = 0)
    end
    s
end
@info part_2(input)
