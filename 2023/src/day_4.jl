# https://adventofcode.com/2023/day/4

input = readlines("2023/data/day_4.txt")
testinput = readlines("2023/data/day_4_test.txt")

function part_1(input)
    s = 0
    for line in input
        ID, winnums, gamenums = match(r"Card\s+(\d+):\s+((?>\d+\s*)+)\|\s+((?>\d+\s*)+)", line).captures
        ID = parse(Int, ID)
        winnums = parse.(Int, split(strip(winnums)))
        gamenums = parse.(Int, split(strip(gamenums)))
        matchingnums = winnums ∩ gamenums
        s += floor(Int, 2.0^(length(matchingnums)-1))
    end
    s
end
@info part_1(input)

function part_2(input)
    s = 0
    copies = ones(Int, length(input))
    for line in input
        ID, winnums, gamenums = match(r"Card\s+(\d+):\s+((?>\d+\s*)+)\|\s+((?>\d+\s*)+)", line).captures
        ID = parse(Int, ID)
        winnums = parse.(Int, split(strip(winnums)))
        gamenums = parse.(Int, split(strip(gamenums)))
        wins = length(winnums ∩ gamenums)
        modifier = length(copies) > 0 ? popfirst!(copies) : 1
        copies[1:wins] += modifier*ones(Int, wins)
        s += modifier
    end
    s
end
@info part_2(input)
