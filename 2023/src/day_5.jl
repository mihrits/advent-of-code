# https://adventofcode.com/2023/day/5

using Chain

input = read("2023/data/day_5.txt", String)[1:end-1] # Removes pesky empty newline in the end
testinput = read("2023/data/day_5_test.txt", String)[1:end-1]

function parse_input(input)
    split_input = split(input, "\n\n")
    seeds = @chain popfirst!(split_input) begin
        split(_, ":")[2]
            strip
        split
        parse.(Int, _)
    end
    seeds, split_input
end

function parse_maps(maps_raw)
    maps = []
    for map_raw in maps_raw
        map_ranges_raw = @chain map_raw begin
            split(_, "\n")[2:end]
            map(strip, _)
            map(split, _)
            map(m -> parse.(Int, m), _)
        end
        f = function (x)
            out = x
            for m in map_ranges_raw
                if x in range(m[2], length=m[3])
                    out = x + m[1] - m[2]
                end
            end
            out
        end
        push!(maps, f)
    end
    ∘(reverse(maps)...)
end

function test(x)
    nothing
end

function part_1(input)
    seeds, maps_raw = parse_input(input)
    mapping = parse_maps(maps_raw)
    minimum(mapping.(seeds))
end
@info part_1(input)

function part_2(input)
    seeds_ranges, maps_raw = parse_input(input)
    mapping = parse_maps(maps_raw)
    seeds = reduce(append!, [collect(range(seeds_ranges[i], length=seeds_ranges[i+1])) for i in 1:2:length(seeds_ranges)])
    minimum(mapping.(seeds))
end
@info part_2(input)
