# https://adventofcode.com/2023/day/6

input = readlines("2023/data/day_6.txt")
testinput = readlines("2023/data/day_6_test.txt")

function wins_for_race(t,s)
    ceil(Int, (t + sqrt(t^2 - 4s))/2 - 1) - floor(Int, (t - sqrt(t^2 - 4s))/2 + 1) + 1
end

function part_1(input)
    times = parse.(Int, split(input[1], keepempty = false)[2:end])
    dists = parse.(Int, split(input[2], keepempty = false)[2:end])
    wins = Int[]
    for race in zip(times, dists)
        push!(wins, wins_for_race(race[1], race[2]))
    end
    prod(wins)
end
@info part_1(input)

function part_2(input)
    time = parse(Int, prod(filter(isdigit, collect(input[1]))))
    dist = parse(Int, prod(filter(isdigit, collect(input[2]))))
    wins_for_race(time, dist)
end
@info part_2(input)
