# https://adventofcode.com/2023/day/7

input = readlines("2023/data/day_7.txt")
testinput = readlines("2023/data/day_7_test.txt")

struct Hand
    cards::Vector{Int8}
    type::Int8
    bid::Int
end

card_to_num = Dict(
    'T' => 0xa,
    'J' => 0xb,
    'Q' => 0xc,
    'K' => 0xd,
    'A' => 0xe,
)

function parse_hands(input)
    hands = Hand[]
    for line in input
        cards_raw, bid_raw = split(line)
        cards = map(x -> isdigit(x) ? parse(Int8, x) : card_to_num[x], collect(cards_raw))
        type = get_handtype(cards)
        bid = parse(Int, bid_raw)
        push!(hands, Hand(cards, type, bid))
    end
    hands
end

function get_handtype(cards)
    counts = [ count(==(c), cards) for c in Set(cards) ]
    type = 0x1
    if 5 in counts
        type = 0x7
    elseif 4 in counts
        type = 0x6
    elseif (3 in counts) && (2 in counts)
        type = 0x5
    elseif 3 in counts
        type = 0x4
    elseif count(==(2), counts) == 2
        type = 0x3
    elseif 2 in counts
        type = 0x2
    end
    type
end

function isless_hand(h1, h2)
    smaller = false
    if h1.type != h2.type
        smaller = isless(h1.type, h2.type)
    elseif h1.cards != h2.cards
        smaller = isless(h1.cards, h2.cards)
    end
    smaller
end

function part_1(input)
    hands = sort!(parse_hands(input), lt=isless_hand)
    bids = map(h -> h.bid, hands)
    sum(prod(e) for e in enumerate(bids))
end
@info part_1(input)

card_to_num2 = Dict(
    'T' => 0xa,
    'J' => 0x0,
    'Q' => 0xc,
    'K' => 0xd,
    'A' => 0xe,
)

function parse_hands2(input)
    hands = Hand[]
    for line in input
        cards_raw, bid_raw = split(line)
        cards = map(x -> isdigit(x) ? parse(Int8, x) : card_to_num2[x], collect(cards_raw))
        type = get_handtype2(cards)
        bid = parse(Int, bid_raw)
        push!(hands, Hand(cards, type, bid))
    end
    hands
end

function get_handtype2(cards)
    counts = [ count(==(c), cards) for c in setdiff(Set(cards), 0x0) ]
    Js = count(==(0x0), cards)
    type = 0x1 # High card
    if Js == 5 # Five of a kind
        type = 0x7
    elseif maximum(counts) + Js == 5 # Five of a kind
        type = 0x7
    elseif maximum(counts) + Js == 4 # Four of a kind
        type = 0x6
    elseif (3 in counts) && (2 in counts) # Full house
        type = 0x5
    elseif count(==(2), counts) == 2 && Js == 1 # Full house
        type = 0x5
    elseif maximum(counts) + Js == 3 # Three of a kind
        type = 0x4
    elseif count(==(2), counts) == 2 # Two pairs
        type = 0x3
    elseif maximum(counts) + Js == 2 # One pair
        type = 0x2
    end
    type
end
function part_2(input)
    hands = sort!(parse_hands2(input), lt=isless_hand)
    bids = map(h -> h.bid, hands)
    sum(prod(e) for e in enumerate(bids))
end
@info part_2(input)
