# www.adventofcode.com 2023 day 7 alternative solutions

input = readlines("2023/data/day_7.txt")
testinput = readlines("2023/data/day_7_test.txt")

struct Hand
    cards::Vector{Int}
    counts::Vector{Int}
    bid::Int
end

cardnums1 = Dict(
    'A' => 14,
    'K' => 13,
    'Q' => 12,
    'J' => 11,
    'T' => 10,
    '9' => 9,
    '8' => 8,
    '7' => 7,
    '6' => 6,
    '5' => 5,
    '4' => 4,
    '3' => 3,
    '2' => 2,
)

cardnums2 = copy(cardnums1)
cardnums2['J'] = 0

function parse_hands(input::Vector{String}, cardnums::Dict, get_counts::Function)
    hands = Hand[]
    for line in input
        cards, bid = split(line)
        cards = map(c -> cardnums[c], collect(cards))
        counts = get_counts(cards)
        bid = parse(Int, bid)
        push!(hands, Hand(cards, counts, bid))
    end
    hands
end

function get_counts1(cards)
    sort!([count(==(card), cards) for card in Set(cards)], rev=true)
end

function get_counts2(cards)
    if cards == zeros(length(cards)) return [length(cards)] end
    counts = sort!([count(==(card), cards) for card in setdiff(Set(cards), 0)], rev=true)
    counts[1] += count(==(0), cards)
    counts
end

function islesshands(h1::Hand, h2::Hand)
    if h1.counts == h2.counts
        return isless(h1.cards, h2.cards)
    end
    isless(h1.counts, h2.counts)
end

function solve(input, cardnums, get_counts)
    hands = sort!(parse_hands(input, cardnums, get_counts), lt=islesshands)
    sum(prod.(enumerate(map(h -> h.bid, hands))))
end
