# https://adventofcode.com/2023/day/8

input = read("2023/data/day_8.txt", String)
testinput1 = read("2023/data/day_8_test1.txt", String)
testinput2 = read("2023/data/day_8_test2.txt", String)
testinput3 = read("2023/data/day_8_test3.txt", String)

function parse_graph(input)
    inst_raw, nodes_raw = split(input, "\n\n")
    inst = map(==('R'), collect(inst_raw)) .+ 1
    nodes = Dict{String, Vector{String}}()
    for line in split(nodes_raw, "\n", keepempty=false)
        e1, e2, e3 = split(line, r" |=|\(|\)|,", keepempty=false)
        push!(nodes, e1 => [e2, e3])
    end
    inst, nodes
end

function part_1(input)
    inst, graph = parse_graph(input)
    currpos = "AAA"
    steps = 0
    while currpos != "ZZZ"
        steps += 1
        currpos = graph[currpos][first(inst)]
        circshift!(inst, -1)
    end
    steps
end
@info part_1(input)

function part_2(input)
    inst, graph = parse_graph(input)
    nodes = collect(keys(graph))
    nodeA_indices = findall(n -> last(n) == 'A', nodes)
    startpos = nodes[nodeA_indices]
    stepses = Int[]
    for currpos in startpos
        steps = 0
        currinst = copy(inst)
        while last(currpos) != 'Z'
            steps += 1
            currpos = graph[currpos][first(currinst)]
            circshift!(currinst, -1)
        end
        push!(stepses, steps)
    end
    lcm(stepses...)
end
@info part_2(input)
