# https://adventofcode.com/2023/day/9

input = readlines("2023/data/day_9.txt")
testinput = readlines("2023/data/day_9_test.txt")

function part_1(input)
    s = 0
    for line in input
        seq = parse.(Int, split(line))
        s += last(seq)
        diffs = diff(seq)
        while !all(diffs .== 0)
            s += last(diffs)
            diffs = diff(diffs)
        end
    end
    s
end
@info part_1(input)

function part_2(input)
    s = 0
    for line in input
        seq = parse.(Int, split(line))
        firstnums = Int[]
        diffs = diff(seq)
        while !all(diffs .== 0)
            pushfirst!(firstnums, first(diffs))
            diffs = diff(diffs)
        end
        s += first(seq) - reduce((a,b) -> b-a, firstnums)
    end
    s
end
@info part_2(input)

function solve(input)
    s1, s2 = 0, 0
    for line in input
        seq = parse.(Int, split(line))
        firstnums = [first(seq)]
        s1 += last(seq)
        diffs = diff(seq)
        while !all(diffs .== 0)
            pushfirst!(firstnums, first(diffs))
            s1 += last(diffs)
            diffs = diff(diffs)
        end
        s2 += reduce((a,b) -> b-a, firstnums)
    end
    s1, s2
end

# This also gives part_2 answer
function part_1r(input)
    s = 0
    for line in input
        seq = reverse(parse.(Int, split(line)))
        s += last(seq)
        diffs = diff(seq)
        while !all(diffs .== 0)
            s += last(diffs)
            diffs = diff(diffs)
        end
    end
    s
end
