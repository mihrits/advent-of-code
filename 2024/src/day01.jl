# Advent of Code 2024 day 1: 

input = read("2024/data/day01.txt", String)

test_input = """3   4
4   3
2   5
1   3
3   9
3   3
"""

function parsing(input)
    m = eachmatch(r"(\d+)", input) |> collect .|> first
    mat = reshape(parse.(Int, m), 2, :) |> permutedims
    mat[:,1], mat[:,2]
end

function part1(input)
    a,b = parsing(input)
    sum(abs.(sort(a) .- sort(b)))
end
@show part1(input)

function part2(input)
    a,b = parsing(input)
    c = [count(==(el), b) for el in a]
    sum(a.*c)
end

