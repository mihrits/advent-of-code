# Advent of Code 2024 day 2

using Accessors

function parse_report(input)
    map(v -> parse.(Int, v), split.(split(input, "\n"; keepempty = false)))
end

function validate(report)
    (issorted(report) || issorted(report, rev = true)) && all(0 .<abs.(diff(report)) .<4)
end

function part1(input)
    sum(validate.(parse_report(input)))
end

function dampen(report)
    validate(report) || any(1:length(report)) do i
        validate(@delete report[i])
    end
end

function part2(input)
    sum(dampen.(parse_report(input)))
end

testinput = """7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9"""

# part 1: 242
# part 2: 311
