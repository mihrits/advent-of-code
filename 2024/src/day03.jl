# Advent of Code 2024 day 3

function part1(memory)
    m = eachmatch(r"mul\((\d{1,3}),(\d{1,3})\)", memory) |> collect
    mapreduce(x -> prod(parse.(Int, x.captures)), +, m)
end

function part2(memory)
    ms = eachmatch(r"mul\((\d{1,3}),(\d{1,3})\)|do\(\)|don't\(\)", memory) |> collect
    d = true
    s = 0
    for m in ms
        if startswith(m.match, "mul")
            s += prod(parse.(Int, m.captures)) * d
        elseif startswith(m.match, "don't")
            d = false
        elseif startswith(m.match, "do")
            d = true
        end
    end
    s
end

testinput = "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"
testinput2 = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"

# part 1: 166357705
# part 2: 88811886
