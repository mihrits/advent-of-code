# Advent of Code 2024 day 4

const CI = CartesianIndex
const CIs = CartesianIndices

function solve(input)
    p = stack(collect.(split(input, "\n", keepempty=false)))
    pc = CIs(p)

    dirs = [CI(i, j) for i in -1:1 for j in -1:1 if (i, j) != (0, 0)]
    allx = findall(==('X'), p)
    s1 = 0
    for x in allx
        for d in dirs
            if x + 3 * d in pc && prod(p[x+i*d] for i in 0:3) == "XMAS"
                s1 += 1
            end
        end
    end

    xdirs = [CI(i, j) for i in (-1, 1) for j in (-1, 1)]
    alla = findall(==('A'), p)
    s2 = 0
    for a in alla
        if all([a + d in pc for d in xdirs])
            if sort(p[a.+xdirs]) == ['M', 'M', 'S', 'S']
                if p[a+xdirs[1]] != p[a+xdirs[4]] # Exclude MAM and SAS crosses
                    s2 += 1
                end
            end
        end
    end
    s1, s2
end

function solve2(input)
    p = stack(collect.(split(input, "\n", keepempty=false)))
    pc = CIs(p)

    dirs = [CI(i, j) for i in -1:1 for j in -1:1 if (i, j) != (0, 0)]
    s1 = sum(count(
        [x + 3 * d in pc && prod(p[x+i*d] for i in 0:3) == "XMAS" for d in dirs]
    )
             for x in findall(==('X'), p)
    )

    xdirs = [CI(i, j) for i in (-1, 1) for j in (-1, 1)]
    s2 = sum(
        all([a + d in pc for d in xdirs])
        && sort(p[a.+xdirs]) == ['M', 'M', 'S', 'S']
        && p[a+xdirs[1]] != p[a-xdirs[1]] # Exclude MAM and SAS crosses
        for a in findall(==('A'), p)
    )
    s1, s2
end

testinput = """MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
"""

# part 1: 2557
# part 2: 1854
